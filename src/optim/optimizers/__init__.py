from torch.optim import Adam, AdamW

from .lars import LARS


def get_optimizer(params, config):
    if config.TRAIN.OPTIMIZATION.OPTIMIZER == 'LARS':
        optimizer = LARS(
            params=params,
            lr=config.TRAIN.OPTIMIZATION.LR,
            momentum=config.TRAIN.OPTIMIZATION.MOMENTUM,
            weight_decay=config.TRAIN.OPTIMIZATION.WEIGHT_DECAY
        )
    elif config.TRAIN.OPTIMIZATION.OPTIMIZER == 'AdamW':
        optimizer = AdamW(
            params=params,
            lr=config.TRAIN.OPTIMIZATION.LR,
            weight_decay=config.TRAIN.OPTIMIZATION.WEIGHT_DECAY
        )
    elif config.TRAIN.OPTIMIZATION.OPTIMIZER == 'Adam':
        optimizer = Adam(
            params=params,
            lr=config.TRAIN.OPTIMIZATION.LR,
            weight_decay=config.TRAIN.OPTIMIZATION.WEIGHT_DECAY
        )
    else:
        raise NotImplementedError(
            f'{config.TRAIN.OPTIMIZATION.OPTIMIZER} optimizer is not yet supported!')
    return optimizer
