from torch import nn


def get_params_and_excluded_params_for_weight_decay(
        model: nn.Module,
        weight_decay: float):
    regularized = []
    not_regularized = []
    names_regularized = []
    names_not_regularized = []
    for name, param in model.named_parameters():
        if not param.requires_grad:
            continue
        # we do not regularize biases nor Norm parameters
        if name.endswith(".bias") or 'bn' in name:
            not_regularized.append(param)
            names_not_regularized.append(name)
        else:
            regularized.append(param)
            names_regularized.append(name)

    weight_decay_params = {'params': regularized, 'weight_decay': weight_decay,
                           'names': names_regularized}
    no_weight_decay_params = {'params': not_regularized, 'weight_decay': 0.0,
                              'names': names_not_regularized}
    return [weight_decay_params, no_weight_decay_params]
