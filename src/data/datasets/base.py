import torch
from torch.utils.data import Dataset
from torchmetrics.classification import MulticlassAccuracy
from torchmetrics import MetricCollection


class ClassificationDataset(Dataset):

    def initialize_eval_metrics(
            self,
            device: torch.device):
        metrics = {}
        for domain in self.domains_count:
            metrics[domain] = MetricCollection({
                'accuracy_micro': MulticlassAccuracy(
                    num_classes=len(self.labels_count), average='micro'),
                'accuracy_macro': MulticlassAccuracy(
                    num_classes=len(self.labels_count), average='macro')}).to(device)
        metrics['overall'] = MetricCollection({
            'accuracy_micro': MulticlassAccuracy(
                num_classes=len(self.labels_count), average='micro'),
            'accuracy_macro': MulticlassAccuracy(
                num_classes=len(self.labels_count), average='macro')}).to(device)
        return metrics
