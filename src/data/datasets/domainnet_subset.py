import os
import json
import numpy as np
from PIL import Image
from math import ceil
from typing import Union, Callable
from dataclasses import dataclass

from .base import ClassificationDataset


@dataclass
class DomainNetSubset(ClassificationDataset):

    domains_count: dict[str, int]
    labels_count: dict[str, int]

    def __init__(
            self,
            data_dir: str,
            split_name: str,
            transform: Callable,
            domains: Union[str, list[str]],
            train_size: Union[float, None] = None,
            seed: Union[int, None] = None):
        """Pytorch dataset for DomainNet subset

        Args:
            data_dir (str): Root directory where the subdirectory 
                domainnet_subset is located.
            split_name (str): split name to used. Possible 
                values among ['train', 'id_val', 'test'] 
            transform (Callable): Transform callback
            domains (list[str]): List of domains used Possible values among
                ['photo', 'art_painting', 'cartoon', 'sketch']
            train_size (Union[float, None], optional): Fraction of the data to 
                use. If None the whole dataset is used
            seed (Union[int, None], optional): Seed use when sampling a 
                fraction of the data.
        """
        js_dataset = self.load_json_dataset(data_dir)
        if isinstance(domains, str):
            domains = [domains]

        if split_name == 'train':
            domain_splits_to_use = ['train', 'test']
        elif split_name == 'id_val':
            domain_splits_to_use = 'test'
        elif split_name == 'test':
            domain_splits_to_use = ['train', 'test']

        self.img_files = []
        self.labels = []
        self.domains = []
        for d in js_dataset:
            if d in domains:
                for domain_split in js_dataset[d]:
                    if domain_split in domain_splits_to_use:
                        for example in js_dataset[d][domain_split]:
                            self.img_files.append(
                                os.path.join(data_dir, 'domainnet_subset', example['img_path']))
                            self.domains.append(d)
                            self.labels.append(example['label'])
        self.img_files = np.array(self.img_files)
        self.domains = np.array(self.domains)
        self.labels = np.array(self.labels)

        # Compute label domain index mapping
        all_domains = ['clipart', 'infograph',
                       'quickdraw', 'painting', 'real', 'sketch']
        self.label2index = {lab: ind for ind,
                            lab in enumerate(np.unique(self.labels))}
        self.index2label = {ind: lab for lab, ind in self.label2index.items()}
        self.domain2index = {dom: ind for ind,
                             dom in enumerate(all_domains)}
        self.index2domain = {ind: dom for dom,
                             ind in self.domain2index.items()}

        self.n_classes = len(self.label2index)
        self.transform = transform

        # Sampling
        if train_size is not None:
            random_state = np.random.RandomState(seed=seed)
            ds_size = len(self.img_files)
            num_samples = ceil(train_size * ds_size)
            indices = random_state.choice(
                np.arange(ds_size), num_samples, replace=False)
            self.img_files = self.img_files[indices]
            self.domains = self.domains[indices]
            self.labels = self.labels[indices]

        self.domains_count = dict(
            (d, c) for d, c in zip(*np.unique(self.domains, return_counts=True)))
        self.labels_count = dict(
            (d, c) for d, c in zip(*np.unique(self.labels, return_counts=True)))

    def load_json_dataset(self, data_dir):
        self.json_path = os.path.join(
            data_dir, 'domainnet_subset', 'domainnet_subset.json')
        self.dataset_name = os.path.basename(os.path.dirname(self.json_path))
        with open(self.json_path, 'r') as f:
            js_dataset = json.load(f)
        return js_dataset

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, idx):
        file = self.img_files[idx]
        img = Image.open(file).convert('RGB')
        imgs = self.transform(img)

        domain = self.domains[idx]
        domain_index = self.domain2index[domain]
        label = self.label2index[self.labels[idx]]
        data = {
            'imgs': imgs,
            'domain': domain,
            'domain_index': domain_index,
            'label': label,
        }
        return data
