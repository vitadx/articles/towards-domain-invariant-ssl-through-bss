import numpy as np
from typing import Union, Callable
from dataclasses import dataclass

import torch
from torch.utils.data import random_split
from wilds import get_dataset

from .base import ClassificationDataset


@dataclass
class Camelyon17WILDS(ClassificationDataset):

    domains_count: dict[str, int]
    labels_count: dict[int, int]

    def __init__(
            self,
            data_dir: str,
            split_name: str,
            transform: Callable,
            train_size: Union[float, None] = None,
            seed: Union[int, None] = None,
            **kwargs):
        """Pytorch dataset for Camelyon17 WILDS

        Args:
            data_dir (str): Root directory where the subdirectory camelyon17 
                is located.
            split_name (str): Split name among ['train', 'id_val', 'val', 
                'test'] 
            transform (Callable): Transform callback
            train_size (Union[float, None], optional): Fraction of the data to
                use. If None the whole dataset is used
            seed (Union[int, None], optional): Seed use when sampling a 
                fraction of the data.
        """
        camelyon17_wilds = get_dataset(
            dataset='camelyon17',
            root_dir=data_dir,
            download=False)
        split = camelyon17_wilds.get_subset(
            split_name, transform=transform)

        # Sampling
        if train_size is not None:
            test_size = 1-train_size
            if seed is not None:
                generator = torch.Generator().manual_seed(seed)
            else:
                generator = torch.Generator()
            split, _ = random_split(
                dataset=split,
                lengths=[train_size, test_size],
                generator=generator)
        self.data = split

        self.domains_count = dict(
            (f'hospital_{d}', c) for d, c in zip(*np.unique(
                self.data.metadata_array[..., self.data.metadata_fields.index(
                    'hospital')], return_counts=True)))
        self.labels_count = dict(
            (d, c) for d, c in zip(
                *np.unique(self.data.y_array, return_counts=True)))

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        sample = self.data[idx]
        img, label, metadata = sample
        data = {
            'imgs': img,
            'label': label,
            'domain': f'hospital_{metadata[0]}',
            'domain_index': metadata[0]
        }
        return data
