import os
import glob
import numpy as np
from PIL import Image
from math import ceil
from typing import Union, Callable
from dataclasses import dataclass

from .base import ClassificationDataset


@dataclass
class PACS(ClassificationDataset):

    domains_count: dict[str, int]
    labels_count: dict[str, int]

    def __init__(
            self,
            data_dir: str,
            split_name: str,
            transform: Callable,
            domains: Union[str, list[str]],
            train_size: Union[float, None] = None,
            seed: Union[int, None] = None,
            **kwargs):
        """Pytorch dataset for PACS subset

        Args:
            data_dir (str): Root directory where the subdirectory 
                PACS is located.
            split_name (str): split name to used. Possible 
                values among ['train', 'id_val', 'test'] 
            transform (Callable): Transform callback
            domains (list[str]): List of domains used Possible values among
                ['photo', 'art_painting', 'cartoon', 'sketch']
            train_size (Union[float, None], optional): Fraction of the data to 
                use. If None the whole dataset is used
            seed (Union[int, None], optional): Seed use when sampling a 
                fraction of the data.
            **kwargs: Extra arguments
        """
        if isinstance(domains, str):
            domains = [domains]

        if split_name == 'train':
            domain_splits_to_use = ['train']
        elif split_name == 'id_val':
            domain_splits_to_use = ['val']
        elif split_name == 'test':
            domain_splits_to_use = ['train', 'val']

        dataset_dir = os.path.join(data_dir, 'PACS')
        all_img_files = glob.glob(
            os.path.join(dataset_dir, '*', '*', '*', '*'))

        self.img_files = []
        self.domains = []
        self.labels = []
        for img_file in all_img_files:
            cls_dir, _ = os.path.split(img_file)
            domain_dir, label = os.path.split(cls_dir)
            split_dir, domain = os.path.split(domain_dir)
            domain_split = os.path.basename(split_dir)
            if domain_split in domain_splits_to_use and domain in domains:
                self.img_files.append(img_file)
                self.domains.append(domain)
                self.labels.append(label)
        self.domains = np.array(self.domains)
        self.labels = np.array(self.labels)

        # Compute label domain index mapping
        all_domains = os.listdir(os.path.join(dataset_dir, 'train'))
        self.label2index = {lab: ind for ind,
                            lab in enumerate(np.unique(self.labels))}
        self.index2label = {ind: lab for lab, ind in self.label2index.items()}
        self.domain2index = {dom: ind for ind,
                             dom in enumerate(all_domains)}
        self.index2domain = {ind: dom for dom,
                             ind in self.domain2index.items()}

        self.n_classes = len(self.label2index)
        self.transform = transform

        # Sampling
        if train_size is not None:
            random_state = np.random.RandomState(seed=seed)
            ds_size = len(self.img_files)
            num_samples = ceil(train_size * ds_size)
            indices = random_state.choice(
                np.arange(ds_size), num_samples, replace=False)
            self.img_files = [self.img_files[idx] for idx in indices]
            self.domains = [self.domains[idx] for idx in indices]
            self.labels = [self.labels[idx] for idx in indices]

        self.domains_count = dict(
            (d, c) for d, c in zip(*np.unique(self.domains, return_counts=True)))
        self.labels_count = dict(
            (d, c) for d, c in zip(*np.unique(self.labels, return_counts=True)))

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, idx):
        file = self.img_files[idx]
        img = Image.open(file).convert('RGB')
        imgs = self.transform(img)

        domain = self.domains[idx]
        domain_index = self.domain2index[domain]
        label = self.label2index[self.labels[idx]]
        data = {
            'imgs': imgs,
            'domain': domain,
            'domain_index': domain_index,
            'label': label,
        }
        return data
