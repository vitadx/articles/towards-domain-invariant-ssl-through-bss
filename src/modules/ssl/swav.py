import torch
from torch import nn, Tensor
from torch.nn import functional as F
from torchvision import models
from torchvision.transforms import functional as TF

from ..heads import ProjectionHead
from ..losses.swav import SWaVLoss


class SWaV(nn.Module):

    def __init__(
            self,
            backbone: str = 'resnet50',
            proj_num_layers: int = 2,
            proj_hidden_dim: int = 4096,
            proj_final_dim: int = 512,
            proj_use_bn: bool = True,
            proj_activation: nn.Module = nn.LeakyReLU,
            imagenet_pretrained: bool = False,
            num_prototypes: int = 256,
            representation_dim: int = 512,
            temperature: float = 0.1,
            eps: float = 0.05,
            freeze_prototypes_niters: int = None,
            **kwargs):
        """Implements the SWaV model from
            'Unsupervised learning of visual features by contrasting cluster assignments'
            Refs:
                @article{caron2020unsupervised,
                    title={Unsupervised learning of visual features by contrasting cluster assignments},
                    author={Caron, Mathilde and Misra, Ishan and Mairal, Julien and Goyal, Priya and Bojanowski, Piotr and Joulin, Armand},
                    journal={Advances in neural information processing systems},
                    volume={33},
                    pages={9912--9924},
                    year={2020}
                    }           

        Args:
            backbone (str, optional): Name of the Resnet backbone. 
                Defaults to 'resnet50'.
            proj_num_layers (int, optional): Number of layers for the 
                projection head. Defaults to 2.
            proj_hidden_dim (int, optional): Hidden dimension of the 
                projection head MLP. Defaults to 4096.
            proj_final_dim (int, optional): Final dimension of the
                projection head MLP. Defaults to 512.
            proj_use_bn (bool, optional): Whether or not to use 
                Batch Normalization  in projeciton head. Defaults to True.
            proj_activation (nn.Module, optional): Activation function for the 
                projection head. Defaults to nn.LeakyReLU.
            imagenet_pretrained (bool, optional): Whether or not to use 
                Imagenet pretraning for backbone. Defaults to False.
            num_prototypes (int, optional): Number of prototypes for the SWaV loss. 
                Defaults to 256.
            representation_dim (int, optional): Dimension of each prototype. 
                Defaults to 512.
            temperature (float, optional): Temperature used for controlling the 
                sharpness of the assignments to prototypes. Defaults to 0.1.
            eps (float, optional): Entropy regulrization hyperparameters. 
                Defaults to 0.05.
            freeze_prototypes_niters (float, optional): Freeze protoytpes during niters first steps 
                of the training. If set to None then prototypes are not frozen 
                at any time. Defaults to None.
        """
        super(SWaV, self).__init__()
        print(f'Using ImageNet pretraing: {imagenet_pretrained}')
        self.imagenet_pretrained = imagenet_pretrained
        self.backbone = models.__dict__[backbone](
            weights="IMAGENET1K_V1" if imagenet_pretrained else None)
        self.backbone.out_features = self.backbone.fc.in_features
        self.backbone.fc = nn.Identity()
        self.projection_head = ProjectionHead(
            input_dim=self.backbone.out_features,
            hidden_dim=proj_hidden_dim,
            final_dim=proj_final_dim,
            num_layers=proj_num_layers,
            use_bn=proj_use_bn,
            activation=proj_activation)
        self.loss_fn = SWaVLoss(
            num_prototypes=num_prototypes,
            representation_dim=representation_dim,
            temperature=temperature,
            eps=eps,
            freeze_prototypes_niters=freeze_prototypes_niters)
        self.loss_names = self.loss_fn.loss_names

    def forward(
        self,
        views: list[Tensor],
        global_step: int,
        **kwargs
    ) -> tuple[dict[str, Tensor], dict[str, Tensor]]:
        """Performs the forward pass of the SimCLR and loss computation

        Args:
            views (Tensor): Batch of augmented views (N, V, 3, H, W) or list
                of several batches of augmented views 
                [(N, V_1, 3, H1, W1), ..., (N, V_K, 3, H2, W2)]. 
                The first batch will be used for the global views while 
                all the rest will be used as local crops. Hence for each image
                V_1 global crops and (V_2, + ... + V_K) local crops.
            global_step (int): Global optimization step. 
                If global step < freeze_prototypes_niters, then prototypes are 
                frozen otherwise they are  unfrozen. 

        Returns:
            tuple[dict[str, Tensor], dict[str, Tensor]]: Backbone and projection head 
                representations + Loss
        """

        # Get backbone and projection representations of each view
        hs = []
        zs = []
        for view in views:
            n, v = view.shape[:2]
            view = view.reshape([-1, *view.shape[-3:]])
            if self.imagenet_pretrained:
                view = TF.normalize(
                    view, mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225])
            h = self.backbone(view)
            z = self.projection_head(h)
            h = h.reshape([n, v, h.size(-1)])
            z = z.reshape([n, v, z.size(-1)])
            hs.append(h)
            zs.append(z)

        z_global_crops = torch.cat(zs[:1], dim=1)
        z_local_crops = torch.cat(zs[1:], dim=1)
        z_global_crops = F.normalize(z_global_crops, p=2, dim=-1)
        z_local_crops = F.normalize(z_local_crops, p=2, dim=-1)
        z_global_crops = z_global_crops.unbind(dim=1)
        z_local_crops = z_local_crops.unbind(dim=1)

        # Computes the SWaV loss
        loss = self.loss_fn(
            z_global_crops=z_global_crops,
            z_local_crops=z_local_crops,
            global_step=global_step,
        )

        outputs = {
            'h': hs,
            'z': zs}
        losses = {
            'loss': loss
        }
        return outputs, losses
