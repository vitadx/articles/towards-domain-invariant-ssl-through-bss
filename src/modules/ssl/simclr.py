from typing import Union

import torch
from torch import nn, Tensor
from torch.nn import functional as F
from torchvision import models
from torchvision.transforms import functional as TF

from ..heads import ProjectionHead
from ..losses.simclr import SupConLoss


class SimCLR(nn.Module):

    def __init__(
            self,
            backbone: str = 'resnet50',
            proj_num_layers: int = 2,
            proj_hidden_dim: int = 4096,
            proj_final_dim: int = 512,
            proj_use_bn: bool = True,
            proj_activation: nn.Module = nn.LeakyReLU,
            imagenet_pretrained: bool = False,
            temperature: float = 0.5,
            **kwargs):
        """Implements the Contrastive Learning method SimCLR presented in:
            A simple framework for contrastive learning of visual representations
            Refs:
                @inproceedings{chen2020simple,
                    title={A simple framework for contrastive learning of visual representations},
                    author={Chen, Ting and Kornblith, Simon and Norouzi, Mohammad and Hinton, Geoffrey},
                    booktitle={International conference on machine learning},
                    pages={1597--1607},
                    year={2020},
                    organization={PMLR}
                    }            

        Args:
            backbone (str, optional): Name of the Resnet backbone. 
                Defaults to 'resnet50'.
            proj_num_layers (int, optional): Number of layers for the 
                projection head. Defaults to 2.
            proj_hidden_dim (int, optional): Hidden dimension of the 
                projection head MLP. Defaults to 4096.
            proj_final_dim (int, optional): Final dimension of the
                projection head MLP. Defaults to 512.
            proj_use_bn (bool, optional): Whether or not to use 
                Batch Normalization  in projeciton head. Defaults to True.
            proj_activation (nn.Module, optional): Activation function for the 
                projection head. Defaults to nn.LeakyReLU.
            imagenet_pretrained (bool, optional): Whether or not to use 
                Imagenet pretraning for backbone. Defaults to False.
            temperature (float, optional): Temperature used in NT-Xent loss. 
                Defaults to 0.5.
        """
        super(SimCLR, self).__init__()
        print(f'Using ImageNet pretraing: {imagenet_pretrained}')
        self.imagenet_pretrained = imagenet_pretrained
        self.backbone = models.__dict__[backbone](
            weights="IMAGENET1K_V1" if imagenet_pretrained else None)
        self.backbone.out_features = self.backbone.fc.in_features
        self.backbone.fc = nn.Identity()
        self.projection_head = ProjectionHead(
            input_dim=self.backbone.out_features,
            hidden_dim=proj_hidden_dim,
            final_dim=proj_final_dim,
            num_layers=proj_num_layers,
            use_bn=proj_use_bn,
            activation=proj_activation)
        self.loss_fn = SupConLoss(
            temperature=temperature
        )
        self.loss_names = self.loss_fn.loss_names

    def forward(
            self,
            views: Union[Tensor, list[Tensor]],
            **kwargs) -> tuple[dict[str, Tensor], dict[str, Tensor]]:
        """Performs the forward pass of the SimCLR and loss computation

        Args:
            views (Tensor): Batch of augmented views (N, V, 3, H, W) or list
                of several batches of augmented views 
                [(N, V1, 3, H1, W1), ..., (N, V2, 3, H2, W2)]

        Returns:
            tuple[dict[str, Tensor], dict[str, Tensor]]: Backbone and projection head 
                representations + Loss
        """
        if isinstance(views, Tensor):
            _views = [views]
        else:
            _views = views

        # Get backbone and projection representations of each view
        hs = []
        zs = []
        for view in _views:
            n, v = view.shape[:2]
            view = view.reshape([-1, *view.shape[-3:]])
            if self.imagenet_pretrained:
                view = TF.normalize(
                    view, mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225])
            h = self.backbone(view)
            z = self.projection_head(h)
            h = h.reshape([n, v, h.size(-1)])
            z = z.reshape([n, v, z.size(-1)])
            hs.append(h)
            zs.append(z)
        hs = torch.cat(hs, dim=1)
        zs = torch.cat(zs, dim=1)

        # Computes the NT-Xent loss
        loss = self.loss_fn(features=F.normalize(input=zs, p=2, dim=-1))

        # If views were fed as list then output representations as list
        if isinstance(views, list):
            hs = hs.split([v.size(1) for v in _views], dim=1)
            zs = zs.split([v.size(1) for v in _views], dim=1)

        outputs = {
            'h': hs,
            'z': zs}
        losses = {
            'loss': loss
        }
        return outputs, losses
