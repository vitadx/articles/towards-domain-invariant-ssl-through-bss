from torch import nn

from .simclr import SimCLR
from .swav import SWaV
from .msn import MSN


def get_ssl_model(config):
    if config.MODEL.SSL_METHOD == 'SimCLR':
        model = SimCLR(
            backbone=config.MODEL.BACKBONE.NAME,
            proj_num_layers=config.MODEL.PROJECTION_HEAD.NUM_LAYERS,
            proj_hidden_dim=config.MODEL.PROJECTION_HEAD.HIDDEN_DIM,
            proj_final_dim=config.MODEL.PROJECTION_HEAD.FINAL_DIM,
            proj_use_bn=config.MODEL.PROJECTION_HEAD.USE_BN,
            proj_activation=nn.LeakyReLU,
            imagenet_pretrained=config.MODEL.BACKBONE.IMAGENET_PRETRAINED,
            temperature=config.TRAIN.OPTIMIZATION.TEMPERATURE,
        )
    elif config.MODEL.SSL_METHOD == 'SWaV':
        model = SWaV(
            backbone=config.MODEL.BACKBONE.NAME,
            proj_num_layers=config.MODEL.PROJECTION_HEAD.NUM_LAYERS,
            proj_hidden_dim=config.MODEL.PROJECTION_HEAD.HIDDEN_DIM,
            proj_final_dim=config.MODEL.PROJECTION_HEAD.FINAL_DIM,
            proj_use_bn=config.MODEL.PROJECTION_HEAD.USE_BN,
            proj_activation=nn.LeakyReLU,
            imagenet_pretrained=config.MODEL.BACKBONE.IMAGENET_PRETRAINED,
            num_prototypes=config.TRAIN.OPTIMIZATION.NUM_PROTOTYPES,
            representation_dim=config.MODEL.PROJECTION_HEAD.FINAL_DIM,
            temperature=config.TRAIN.OPTIMIZATION.TEMPERATURE,
            eps=config.TRAIN.OPTIMIZATION.ENTROPY_REGULARIZATION,
            freeze_prototypes_niters=config.TRAIN.OPTIMIZATION.FREEZE_PROTOTYPES_NITERS
        )
    elif config.MODEL.SSL_METHOD == 'MSN':
        model = MSN(
            backbone=config.MODEL.BACKBONE.NAME,
            proj_num_layers=config.MODEL.PROJECTION_HEAD.NUM_LAYERS,
            proj_hidden_dim=config.MODEL.PROJECTION_HEAD.HIDDEN_DIM,
            proj_final_dim=config.MODEL.PROJECTION_HEAD.FINAL_DIM,
            proj_use_bn=config.MODEL.PROJECTION_HEAD.USE_BN,
            proj_activation=nn.GELU,
            num_prototypes=config.TRAIN.OPTIMIZATION.NUM_PROTOTYPES,
            temperature=config.TRAIN.OPTIMIZATION.TEMPERATURE,
            regularization_weight=config.TRAIN.OPTIMIZATION.REGULARIZATION_WEIGHT,
            ema_momentum=config.TRAIN.OPTIMIZATION.EMA_MOMENTUM,
            freeze_prototypes_niters=config.TRAIN.OPTIMIZATION.FREEZE_PROTOTYPES_NITERS
        )
    else:
        raise NotImplementedError(
            f'{config.MODEL.SSL_METHOD} has not been implemented!')
    return model
