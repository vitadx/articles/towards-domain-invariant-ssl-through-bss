import torch
from torch import nn, Tensor

from ..backbones import deit
from ..utils.ema import EMAN
from ..heads import ProjectionHead
from ..losses.msn import MSNLoss


class MSN(nn.Module):

    def __init__(
            self,
            backbone: str = 'deit_small_p16',
            proj_num_layers: int = 2,
            proj_hidden_dim: int = 4096,
            proj_final_dim: int = 512,
            proj_use_bn: bool = False,
            proj_activation: nn.Module = nn.GELU,
            patch_masking_ratio: float = 0.75,
            num_prototypes: int = 256,
            temperature: float = 0.1,
            regularization_weight: float = 10,
            ema_momentum: float = 0.995,
            freeze_prototypes_niters: int = None,
            **kwargs):
        """Implements the SWaV model from
            'Unsupervised learning of visual features by contrasting cluster assignments'
            Refs:
                @article{caron2020unsupervised,
                    title={Unsupervised learning of visual features by contrasting cluster assignments},
                    author={Caron, Mathilde and Misra, Ishan and Mairal, Julien and Goyal, Priya and Bojanowski, Piotr and Joulin, Armand},
                    journal={Advances in neural information processing systems},
                    volume={33},
                    pages={9912--9924},
                    year={2020}
                    }           

        Args:
            backbone (str, optional): Name of the Resnet backbone. 
                Defaults to 'resnet50'.
            proj_num_layers (int, optional): Number of layers for the 
                projection head. Defaults to 2.
            proj_hidden_dim (int, optional): Hidden dimension of the 
                projection head MLP. Defaults to 4096.
            proj_final_dim (int, optional): Final dimension of the
                projection head MLP. Defaults to 512.
            proj_use_bn (bool, optional): Whether or not to use 
                Batch Normalization  in projeciton head. Defaults to True.
            proj_activation (nn.Module, optional): Activation function for the 
                projection head. Defaults to nn.LeakyReLU.
            patch_masking_ratio (float, optional): Patch masking ratio for the random views. 
                Defaults to 0.75.
            num_prototypes (int, optional): Number of prototypes for the SWaV loss. 
                Defaults to 256.
            temperature (float, optional): Temperature used for controlling the 
                sharpness of the assignments to prototypes. Defaults to 0.1.
            regularization_weight (float, optional): Entropy regularization weight. 
                Defaults to 10.
            regularization_weight (float, optional): Model exponential moving average
                momentum. Defaults to 0.995.
            freeze_prototypes_niters (float, optional): Freeze protoytpes during niters first steps 
                of the training. If set to None then prototypes are not frozen 
                at any time. Defaults to None.
        """
        super(MSN, self).__init__()
        self.backbone = deit.__dict__[backbone]()
        self.backbone.fc = nn.Identity()
        self.backbone.pred = ProjectionHead(
            input_dim=self.backbone.out_features,
            hidden_dim=proj_hidden_dim,
            final_dim=proj_final_dim,
            num_layers=proj_num_layers,
            use_bn=proj_use_bn,
            activation=proj_activation)
        self.backbone_ema = EMAN(
            self.backbone, momentum=ema_momentum).eval()
        # self.backbone_ema.ema_model.pred = nn.Identity()
        self.patch_masking_ratio = patch_masking_ratio
        self.loss_fn = MSNLoss(
            num_prototypes=num_prototypes,
            prototypes_dim=proj_final_dim,
            temperature=temperature,
            sinkhorn_iterations=7,
            regularization_weight=regularization_weight,
            freeze_prototypes_niters=freeze_prototypes_niters,
            gather_distributed=True,
        )
        self.loss_names = self.loss_fn.loss_names

    def forward(
        self,
        views: list[Tensor],
        global_step: int,
        **kwargs
    ) -> tuple[dict[str, Tensor], dict[str, Tensor]]:
        """Performs the forward pass of the SimCLR and loss computation

        Args:
            views (Tensor): Batch of augmented views (N, V, 3, H, W) or list
                of several batches of augmented views 
                [(N, V_1, 3, H1, W1), ..., (N, V_K, 3, H2, W2)]. 
                The first batch will be used for the global views while 
                all the rest will be used as local crops. Hence for each image
                V_1 global crops and (V_2, + ... + V_K) local crops.
            global_step (int): Global optimization step. 
                If global step < freeze_prototypes_niters, then prototypes are 
                frozen otherwise they are  unfrozen. 

        Returns:
            tuple[dict[str, Tensor], dict[str, Tensor]]: Backbone and projection head 
                representations + Loss
        """
        if self.training:
            self.backbone_ema.update_ema_weights(self.backbone)

        # Get backbone and projection representations of each view
        # hs = []
        zs = []
        for view_index, view in enumerate(views):
            n, v = view.shape[:2]
            view = view.reshape([-1, *view.shape[-3:]])
            if view_index == 0:
                # Processing target views
                z = self.backbone_ema(view).detach()
            elif view_index == 1:
                # Processing random views
                z = self.backbone(view, patch_drop=self.patch_masking_ratio)
            else:
                # Processing focal views
                z = self.backbone(view)

            z = z.reshape([n, v, z.size(-1)])
            zs.append(z)

        # h_target_views, *_ = hs
        z_target_views, z_random_views, z_focal_views = zs

        z_target_views = z_target_views.flatten(0, 1)
        z_anchors = torch.cat(
            [z_random_views, z_focal_views], dim=1).flatten(0, 1)

        # Computes the MSN loss
        losses = self.loss_fn(
            anchors=z_anchors,
            targets=z_target_views,
            target_sharpen_temperature=0.25,
            global_step=global_step
        )
        outputs = {
            # 'h': hs,
            'z': zs,
        }
        return outputs, losses
