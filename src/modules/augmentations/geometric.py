import torch
from torch import nn
from kornia import augmentation as K


class GeometricAugmentation(nn.Module):

    def __init__(self,
                 resized_shape: tuple[int, int],
                 scale: float,
                 vertical_flip: bool,
                 degrees: int,
                 erase_rate: float):
        """Geometric augmentations module

        Args:
            resized_shape (tuple[int, int]): Random crop resized shape
            scale (float): Ration between crop and full image
            vertical_flip (bool): Whether or not apply random vertical flip
            degrees (int): Degrees for the random rotations. 
                Random rotations will be in the range (-degrees, degrees)
            erase_rate (float): Maximum portion of the image to erase when using
                random erasing.
        """
        super().__init__()
        self.aug = nn.Sequential(
            K.RandomResizedCrop(
                p=1,
                size=resized_shape,
                scale=scale, same_on_batch=False),
            K.RandomVerticalFlip(p=0.5, same_on_batch=False) if vertical_flip else
            nn.Identity(),
            K.RandomHorizontalFlip(p=0.5, same_on_batch=False),
            K.RandomAffine(degrees=degrees, p=1, same_on_batch=False),
            K.RandomErasing([0, erase_rate], p=1, same_on_batch=False),
        )

    @torch.no_grad()
    def forward(self, x):
        """Forward pass of the geometric augmentation module

        Args:
            x (torch.tensor): Batch of images (N, 3, H, W) or (N, V, 3, H, W) 

        Returns:
            torch.Tensor: Augmented images 
        """
        if x.ndim == 5:
            n, views, c, h, w = x.shape
            x = x.reshape([-1, c, h, w])
            x = self.aug(x)
            x = x.reshape([n, views, c, *x.shape[-2:]])
        elif x.ndim == 4:
            x = self.aug(x)
        return x
