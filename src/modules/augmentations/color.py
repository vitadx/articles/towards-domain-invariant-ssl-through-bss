import torch
from torch import nn
from kornia import augmentation as K


class ColorAugmentation(nn.Module):

    def __init__(self,
                 strength: str = 'light',
                 same_on_batch: bool = True,
                 ):
        """Color augmentations module

        Args:
            strength (str, optional): Strength of the color augmentation 
            ('light', 'strong'). Defaults to 'light'.
            same_on_batch (bool, optional): Whether to apply the same color 
            augmentation on the whole batch. Defaults to True.
        """
        super().__init__()
        if strength == 'light':
            self.aug = nn.Sequential(
                K.ColorJitter(
                    brightness=0.2, contrast=0.2, saturation=0.2, hue=0.2,
                    same_on_batch=same_on_batch, p=1.0))
        elif strength == 'strong':
            self.aug = nn.Sequential(
                K.ColorJitter(
                    brightness=0.2, contrast=0.2, saturation=0.2, hue=0.2,
                    same_on_batch=same_on_batch, p=1.0),
                K.RandomEqualize(same_on_batch=same_on_batch, p=0.1),
                K.RandomPosterize(bits=torch.tensor(
                    6,
                    # device=device
                ),
                    same_on_batch=same_on_batch, p=0.1),
                K.RandomSolarize(
                    thresholds=0.1, additions=0.1,
                    same_on_batch=same_on_batch, p=0.1),
                K.RandomGrayscale(same_on_batch=same_on_batch, p=0.1),
            )

    @torch.no_grad()
    def forward(self, x: torch.Tensor):
        """Forward pass of the color augmentation module

        Args:
            x (torch.tensor): Batch of images (N, 3, H, W) or (N, V, 3, H, W) 

        Returns:
            torch.Tensor: Augmented images 
        """
        if x.ndim == 5:
            for col_index in torch.arange(x.size(1)):
                x[:, col_index, ...] = self.aug(x[:, col_index, ...])
        elif x.ndim == 4:
            x = self.aug(x)
        return x
