from matplotlib import pyplot as plt

from prodict import Prodict
import torch
from torch import nn
from torchvision.utils import make_grid
from torchvision.transforms import functional as TF

from .geometric import GeometricAugmentation
from .color import ColorAugmentation
from .fourier import MultiViewsFourierAugmentation, BatchStylesStandardization


class AugmentationPipeline(nn.Module):
    """Augmentation pipeline to build several batches of augmented views based 
        on a batch of images
    """

    def __init__(
            self,
            config: Prodict):
        super().__init__()
        self.fourier_based_augs = nn.ModuleList()
        self.color_augs = nn.ModuleList()
        self.geometrics_augs = nn.ModuleList()
        self.number_views = config.TRAIN.TRANSFORMS.MULTI_CROPS.NUM_VIEWS

        # Define fourier based augmentation (fourier/BSS) for each
        # multi-views batch
        if 'FOURIER' in config.TRAIN.TRANSFORMS.MULTI_CROPS:
            for bss, areas_ratio, in zip(
                    config.TRAIN.TRANSFORMS.MULTI_CROPS.FOURIER.BSS,
                    config.TRAIN.TRANSFORMS.MULTI_CROPS.FOURIER.AREAS_RATIOS):
                fourier_aug = BatchStylesStandardization(areas_ratios=areas_ratio) if \
                    bss else MultiViewsFourierAugmentation(
                    areas_ratios=areas_ratio)
                self.fourier_based_augs.append(fourier_aug)
        else:
            for _ in config.TRAIN.TRANSFORMS.MULTI_CROPS.NUM_VIEWS:
                self.fourier_based_augs.append(nn.Identity())

        # Define color augmentations for each  multi-views batch
        self.color_augs.extend(
            [ColorAugmentation(strength=strength, same_on_batch=same) for
                strength, same in zip(
                    config.TRAIN.TRANSFORMS.MULTI_CROPS.COLOR.STRENGTHS,
                    config.TRAIN.TRANSFORMS.MULTI_CROPS.COLOR.SAME_ON_BATCH)])

        # Define geometric augmentations for each multi-views batch
        geometric_aug_config = config.TRAIN.TRANSFORMS.MULTI_CROPS.GEOMETRIC
        self.geometrics_augs.extend(
            [GeometricAugmentation(
                resized_shape=resized_shape,
                scale=scale,
                vertical_flip=vertical_flip,
                degrees=degrees,
                erase_rate=erase_rate,
            ) for resized_shape, scale, vertical_flip, degrees, erase_rate in
                zip(geometric_aug_config.RESIZED_SHAPES, geometric_aug_config.SCALES,
                    geometric_aug_config.VERTICAL_FLIPS, geometric_aug_config.ROTATIONS_DEGREES,
                    geometric_aug_config.ERASE_RATES,
                    )])

    @torch.no_grad()
    def forward(
            self,
            imgs: torch.Tensor):
        """Forward pass of the augmentation pipeline. Pipeline augmentation consists in the following steps:
            1. Fourier based augmentations (FA or BSS)
            2. Color augmentations
            3. Geometric augmentaitions

        Args:
            imgs (torch.Tensor): Batch of images (N, 3, H, W)

        Returns:
            list[torch.Tensor]: List of batches with several augmented views:
                [(N, V_1, 3, H, W), ..., (N, V_K, 3, H, W)]
        """
        aug_imgs = []
        for n_views, fourier_based_aug, color_aug, geometric_aug in zip(
                self.number_views, self.fourier_based_augs, self.color_augs, self.geometrics_augs):
            if not isinstance(fourier_based_aug, nn.Identity):
                aug_img = fourier_based_aug(imgs, n_views=n_views)
            else:
                aug_img = imgs
            aug_img = color_aug(aug_img)
            aug_img = geometric_aug(aug_img)
            aug_imgs.append(aug_img)
        return aug_imgs

    def visualize_pipeline(self, imgs):
        aug_imgs = self.forward(imgs)
        fig, axes = plt.subplots(
            1, len(aug_imgs), squeeze=False, figsize=(len(aug_imgs) * 8, aug_imgs[0].shape[0]))
        for view_index, views in enumerate(aug_imgs):
            grid = make_grid(tensor=views.reshape(
                [-1, *views.shape[-3:]]), nrow=views.size(1))
            grid = TF.to_pil_image(grid)
            axes[0, view_index].imshow(grid)
        return fig, axes
