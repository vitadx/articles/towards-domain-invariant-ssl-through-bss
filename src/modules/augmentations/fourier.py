import random

import torch
from torch import nn


class FourierAugmentation(nn.Module):
    """Implements Fourier Based Augmentation. Given a
        batch of N images and their Fourier transforms,
        we manipulate the different amplitudes by
        substituting their low-frequency components
        with those of random images.

    Attributes:
        areas_ratios (tuple): $(r_min, r_max)$ specifying
            the minimum and maximum possible
            areas ratio between the substituted
            amplitude and the full amplitude.
    """

    def __init__(self, areas_ratios):
        super().__init__()
        self.areas_ratios = areas_ratios

    def substitute_low_freq(
            self,
            amp_src: torch.Tensor,
            amp_tgt: torch.Tensor,
            area_ratio: float):
        """Substitute the low-frequency components of the source amplitudes 
        with those of the target amplitudes.

        Args:
            amp_src (torch.Tensor): Source amplitudes (N, ..., 3, H, W)
            amp_tgt (torch.Tensor): Target amplitudes (N, ..., 3, H, W)
            area_ratio (float): Area ratio between the substituted amplitude and the
                full amplitude.

        Returns:
            torch.Tensor: Source amplitudes where the low-frequency components 
            have been substituted with those of the target amplitudes.
        """
        # Compute center coordinates of amplitudes
        h, w = amp_src.shape[-2:]
        hc, wc = int(h//2), int(w//2)

        # Compute half length `l` of the components to be substituted
        len_amp = min([int(area_ratio*h/2), int(area_ratio*w/2)])

        # Substitute low freq components of source
        # amplitudes with those of the target amplitudes
        low_freq_tgt = amp_tgt[..., hc-len_amp:hc +
                               len_amp, wc-len_amp:wc+len_amp]
        new_low_freq = low_freq_tgt
        amp_src[..., hc-len_amp:hc+len_amp, wc -
                len_amp:wc+len_amp] = new_low_freq
        return amp_src

    @torch.no_grad()
    def __call__(self, imgs: torch.Tensor):
        """Performs Fourier augmentations on a batch of imgs

        Args:
            imgs (torch.Tensor): Batch of images (N, 3, H, W)

        Returns:
            torch.Tensor: Batch of images augmented with Fourier Augmentation
                (N, 3, H, W)
        """

        # Apply FFT on source images
        fft_src = torch.fft.fftn(imgs, dim=(-2, -1))
        # Shift low-frequency components to the center
        fft_src = torch.fft.fftshift(fft_src, dim=(-2, -1))
        # Retrieve amplitude and phase
        src_amp, src_phase = fft_src.abs(), fft_src.angle()
        # For each src amplitudes, sample randomly a target amplitudes
        tgt_indexes = torch.randperm(imgs.size(0))
        tgt_amp = src_amp[tgt_indexes]
        # Substitute low-freq of src amplitudes with those
        area_ratio = random.uniform(*self.areas_ratios)
        src_amp = self.substitute_low_freq(
            src_amp, tgt_amp, area_ratio)
        # Reconstruct FFT from amp and phase
        fft_src = torch.polar(src_amp, src_phase)
        # Shift back low-frequency to their original positions
        fft_src = torch.fft.ifftshift(fft_src, dim=(-2, -1))
        # Invert FFT
        new_imgs = torch.fft.ifftn(fft_src, dim=(-2, -1)).real.clamp(0, 1)
        return new_imgs


class MultiViewsFourierAugmentation(FourierAugmentation):
    """Implements Multiple Views Fourier Based Augmentation. 
        Given a batch of N images and their Fourier transforms,
        we manipulate the different amplitudes by
        substituting their low-frequency components
        with those of random images. This process is repeated several times to
        construct several augmented views.

    Attributes:
        areas_ratios (tuple): $(r_min, r_max)$ specifying
            the minimum and maximum possible
            areas ratio between the substituted
            amplitude and the full amplitude.
    """

    @torch.no_grad()
    def __call__(self, imgs: torch.Tensor, n_views: int):
        """Performs Fourier augmentations on a batch of imgs several times to
            construct several augmented views. 

        Args:
            imgs (torch.Tensor): Batch of images (N, 3, H, W)
            n_views(int): Number of views to generate

        Returns:
            torch.Tensor: Images augmented with Fourier Augmentation with n_views
                (N, n_views, 3, H, W)
        """
        # Apply FFT on source images
        fft_src = torch.fft.fftn(imgs, dim=(-2, -1))
        # Shift low-frequency components to the center
        fft_src = torch.fft.fftshift(fft_src, dim=(-2, -1))
        # Retrieve amplitude and phase
        src_amp, src_phase = fft_src.abs(), fft_src.angle()

        # Apply Fourier augmentation n_views times
        new_imgs = []
        for _ in range(n_views):

            # For each src amplitudes, sample randomly a target amplitudes
            tgt_indexes = torch.randperm(imgs.size(0))
            tgt_amp = src_amp[tgt_indexes]

            # Substitute low-freq of src amplitudes with those
            areas_ratios = random.uniform(*self.areas_ratios)
            aug_amp = self.substitute_low_freq(src_amp, tgt_amp, areas_ratios)

            # Reconstruct FFT from amp and phase
            fft_aug = torch.polar(aug_amp, src_phase)
            # Shift back low-frequency to their original positions
            fft_aug = torch.fft.ifftshift(fft_aug, dim=(-2, -1))
            # Invert FFT and append augmented images
            new_imgs.append(
                torch.fft.ifftn(fft_aug, dim=(-2, -1)).real.clamp(0, 1))

        # Stack all the augmented images
        new_imgs = torch.stack(new_imgs, dim=1)
        return new_imgs


class BatchStylesStandardization(FourierAugmentation):
    """Implements Batch Styles Standardization. Given a
        batch of N images and their Fourier transforms,
        we manipulate the different amplitudes by
        substituting their low-frequency components
        with those of a single randomly chosen image.

    Attributes:
        areas_ratios (tuple): $(r_min, r_max)$ specifying
            the minimum and maximum possible
            areas ratio between the substituted
            amplitude and the full amplitude.
    """

    @torch.no_grad()
    def __call__(self, imgs, n_views):
        """Apply batch styles standardization `n_views` times 
            on a batch of $N$ images.

        Args:
            imgs (torch.Tensor): Batch of images (N, 3, H, W)
            n_views (int): Number of augmented views

        Returns:
            torch.Tensor: Batch with standardized styles 
            (N, n_views, 3, H, W)
        """
        # Apply FFT on source images
        fft = torch.fft.fftn(
            imgs, dim=(-2, -1))
        # Shift low-frequency components to the center
        fft = torch.fft.fftshift(
            fft, dim=(-2, -1))
        # Retrieve amplitude and phase
        amp, phase = fft.abs(), fft.angle()

        # Sample n_views images that will be used as
        # ref styles
        bs = imgs.size(0)
        if n_views <= bs:
            sampled_ind = torch.randperm(bs)[:n_views]
        else:
            sampled_ind = torch.randint(0, bs, (n_views,))

        # Substitute low-freq of src amplitudes with those
        # of the n_views sampled images
        src_amp = amp.unsqueeze(1).repeat(
            [1, n_views, 1, 1, 1])
        tgt_amp = amp[sampled_ind].unsqueeze(0).expand(
            bs, -1, -1, -1, -1)
        sampled_ratio = random.uniform(*self.areas_ratios)
        amp = self.substitute_low_freq(
            src_amp, tgt_amp, sampled_ratio)

        phase = phase.unsqueeze(1)
        # Reconstruct FFT from amp and phase
        fft = torch.polar(amp, phase)
        # Shift back low-frequency to their
        # original positions
        fft = torch.fft.ifftshift(fft, dim=(-2, -1))
        # Invert FFT
        imgs = torch.fft.ifftn(
            fft, dim=(-2, -1)).real.clamp(0, 1)
        return imgs
