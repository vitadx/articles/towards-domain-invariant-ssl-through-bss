import torch
import torch.distributed as dist


def all_gather(embeddings):
    # Gather the embeddings from every replica.
    embeddings_list = [torch.ones_like(embeddings)
                       for _ in range(dist.get_world_size())]
    dist.all_gather(embeddings_list, embeddings)

    # Gathered tensors have no gradient, so we overwrite the gathered tensor for the current replica with the embeddings produced on this replica, which do have gradients.
    embeddings_list[dist.get_rank()] = embeddings

    # Finally, concatenate the list of embeddings before computing a loss.
    embeddings = torch.cat(embeddings_list)
    return embeddings
