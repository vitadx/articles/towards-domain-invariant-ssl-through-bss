import copy

from collections import OrderedDict

import torch
from torch import nn


class EMA(nn.Module):
    def __init__(self, model: nn.Module, momentum: float):
        super().__init__()
        self.momentum = momentum
        self.ema_model = copy.deepcopy(model)

        for param in self.ema_model.parameters():
            param.detach_()
            param.requires_grad_(False)

    @torch.no_grad()
    def update_ema_weights(self, current_model):
        model_params = OrderedDict(current_model.named_parameters())
        ema_model_params = OrderedDict(self.ema_model.named_parameters())
        model_buffers = OrderedDict(current_model.named_buffers())
        ema_model_buffers = OrderedDict(self.ema_model.named_buffers())
        # check if both model contains the same set of keys
        # assert model_params.keys() == ema_model_params.keys()

        for name in ema_model_params:
            # see https://www.tensorflow.org/api_docs/python/tf/train/ExponentialMovingAverage
            # shadow_variable -= (1 - momentum) * (shadow_variable - variable)
            ema_model_params[name].sub_(
                (1. - self.momentum) * (ema_model_params[name] - model_params[name]))

        # check if both model contains the same set of keys
        # assert model_buffers.keys() == ema_model_buffers.keys()

        for name in ema_model_buffers:
            # buffers are copied
            ema_model_buffers[name].copy_(model_buffers[name])

    @torch.no_grad()
    def forward(self, inputs, *args, **kwargs):
        return self.ema_model(inputs, *args, **kwargs)


class EMAN(EMA):

    @torch.no_grad()
    def update_ema_weights(self, current_model):
        model_params = OrderedDict(current_model.named_parameters())
        ema_model_params = OrderedDict(self.ema_model.named_parameters())
        model_buffers = OrderedDict(current_model.named_buffers())
        ema_model_buffers = OrderedDict(self.ema_model.named_buffers())
        # check if both model contains the same set of keys
        # assert model_params.keys() == ema_model_params.keys()

        for name in ema_model_params:
            # see https://www.tensorflow.org/api_docs/python/tf/train/ExponentialMovingAverage
            # shadow_variable -= (1 - momentum) * (shadow_variable - variable)
            ema_model_params[name].sub_(
                (1. - self.momentum) * (ema_model_params[name] - model_params[name]))

        # check if both model contains the same set of keys
        # assert model_buffers.keys() == ema_model_buffers.keys()

        for name in ema_model_buffers:
            if 'num_batches_tracked' in name:
                # buffers are copied
                ema_model_buffers[name].copy_(model_buffers[name])
            else:
                ema_model_buffers[name].sub_(
                    (1. - self.momentum) * (ema_model_buffers[name] - model_buffers[name]))
