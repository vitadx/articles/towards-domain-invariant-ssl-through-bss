from typing import Callable

from torch import nn


class ProjectionHead(nn.Module):

    def __init__(
            self,
            input_dim: int,
            hidden_dim: int = 4096,
            final_dim: int = 512,
            num_layers: int = 2,
            use_bn: bool = True,
            activation: Callable[..., nn.Module] = nn.LeakyReLU):
        super(ProjectionHead, self).__init__()
        projection_head_layers = []
        for i in range(num_layers-1):
            is_first_layer = i == 0
            in_dim = input_dim if is_first_layer else hidden_dim
            projection_head_layers.append(
                nn.Sequential(
                    nn.Linear(in_dim, hidden_dim, bias=not use_bn),
                    nn.BatchNorm1d(
                        hidden_dim) if use_bn else nn.Identity(),
                    activation()))
        projection_head_layers.append(
            nn.Linear(hidden_dim, final_dim))
        self.head = nn.Sequential(*projection_head_layers)

    def forward(self, x):
        return self.head(x)
