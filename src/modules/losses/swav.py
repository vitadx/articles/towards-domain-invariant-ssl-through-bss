import torch
from torch import nn
import torch.distributed as dist
from torch.nn import functional as F


class SWaVLoss(nn.Module):

    def __init__(
            self,
            num_prototypes: int,
            representation_dim: int,
            temperature: float,
            eps: float = 0.05,
            freeze_prototypes_niters: int = None):
        """SWaV Loss proposed: 
            https://proceedings.neurips.cc/paper_files/paper/2020/file/70feb62b69f16e0238f741fab228fec2-Paper.pdf

        Args:
            num_prototypes (int): Number of prototypes
            representation_dim (int): Prototype dimension
            temperature (float): Temperature
            eps (float, optional): Entropy Regularization term.  
                Defaults to 0.05.
            freeze_prototypes_niters (float, optional): Freeze protoytpes during niters first steps 
                of the training. If set to None then prototypes are not frozen 
                at any time. Defaults to None.
        """
        super().__init__()
        self.num_prototypes = num_prototypes
        self.representation_dim = representation_dim
        self.temperature = temperature
        self.prototypes = nn.Linear(
            num_prototypes, representation_dim, bias=False).weight
        self.eps = eps
        self.freeze_prototypes_niters = freeze_prototypes_niters
        self.loss_names = ['loss']

    def forward(self,
                z_global_crops: list[torch.Tensor],
                z_local_crops: list[torch.Tensor],
                global_step: int):
        """Compute the loss SWaV loss.

        Args:
            z_global_crops (list[torch.Tensor]): List of tensors of shape
                (N, D) corresponding to normalized projection head
                representations associated to high resolution crops for 
                computing the codes. 
            z_local_crops (list[torch.Tensor]): List of tensors of shape
                (N, D) corresponding to normalized projection head
                representations associated to low resolution crops for
                soft codes.
            global_step (int): Global optimization step. 
                If global step < freeze_prototypes_niters, then prototypes are 
                frozen otherwise they are  unfrozen. 
        """
        self.freeze_prototypes(global_step=global_step)
        prototypes = F.normalize(self.prototypes, dim=0, p=2)

        # Compute score for low and high res crops representations
        scores_high_res_crops = [
            z_high_res_crop @ prototypes for z_high_res_crop in z_global_crops]
        scores_low_res_crops = [
            z_low_res_crop @ prototypes for z_low_res_crop in z_local_crops]

        # Compute codes for high res crops
        with torch.no_grad():
            codes_high_res_crops = [self.distributed_sinkhorn(
                s_high_res_crop.detach()) for s_high_res_crop in scores_high_res_crops]

        # Convert scores to probabilities forlow res crops
        logits_low_res_crops = [
            scores_low_res_crop / self.temperature for scores_low_res_crop in scores_low_res_crops]
        logits_high_res_crops = [
            scores_high_res_crop / self.temperature for scores_high_res_crop in scores_high_res_crops]

        losses = []
        # Match soft codes of low crops to hard codes of high res crops
        for q in codes_high_res_crops:
            for x in logits_low_res_crops:
                subloss = - torch.mean(
                    torch.sum(q * F.log_softmax(x, dim=1), dim=1))
                losses.append(subloss)

        # Match soft codes of high res crops to hard codes of high res crops
        for codes_index, q in enumerate(codes_high_res_crops):
            for logits_index, x in enumerate(logits_high_res_crops):
                if logits_index != codes_index:
                    subloss = - torch.mean(
                        torch.sum(q * F.log_softmax(x, dim=1), dim=1))
                    losses.append(subloss)

        loss = torch.stack(losses).mean()
        return loss

    # Sinkhorn-Knopp
    @torch.no_grad()
    def distributed_sinkhorn(self, out, iters=3, use_dist=True):
        _got_dist = use_dist and torch.distributed.is_available() \
            and torch.distributed.is_initialized() \
            and (torch.distributed.get_world_size() > 1)
        if _got_dist:
            world_size = torch.distributed.get_world_size()
        else:
            world_size = 1

        # Q is K-by-B for consistency with notations from our paper
        Q = torch.exp(out / self.eps).t()
        B = Q.shape[1] * world_size  # number of samples to assign
        K = Q.shape[0]  # how many prototypes

        # make the matrix sums to 1
        sum_Q = torch.sum(Q)
        dist.all_reduce(sum_Q)
        Q /= sum_Q

        for _ in range(iters):
            # normalize each row: total weight per prototype must be 1/K
            sum_of_rows = torch.sum(Q, dim=1, keepdim=True)
            dist.all_reduce(sum_of_rows)
            Q /= sum_of_rows
            Q /= K

            # normalize each column: total weight per sample must be 1/B
            Q /= torch.sum(Q, dim=0, keepdim=True)
            Q /= B

        Q *= B  # the colomns must sum to 1 so that Q is an assignment
        return Q.t()

    def freeze_prototypes(self, global_step):
        if self.freeze_prototypes_niters is not None:
            proto_requires_grad = global_step >= self.freeze_prototypes_niters
        else:
            proto_requires_grad = True
        self.prototypes.requires_grad_(proto_requires_grad)
