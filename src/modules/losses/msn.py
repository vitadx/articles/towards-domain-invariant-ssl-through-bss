import math

import torch
from torch import nn
import torch.distributed as dist
from torch.nn import functional as F


class MSNLoss(nn.Module):
    """Implementation of the loss function from MSN [0].

    Code inspired by [1].

    - [0]: Masked Siamese Networks, 2022, https://arxiv.org/abs/2204.07141
    - [1]: https://github.com/facebookresearch/msn

    Attributes:
        temperature:
            Similarities between anchors and targets are scaled by the inverse of
            the temperature. Must be in (0, inf).
        sinkhorn_iterations:
            Number of sinkhorn normalization iterations on the targets.
        regularization_weight:
            Weight factor lambda by which the regularization loss is scaled. Set to 0
            to disable regularization.
        freeze_prototypes_niters:
            When global step < freeze_prototypes_niters, prototypes are frozen 
                otherwise they are unfrozen, By default, they are always unfrozen
        me_max_weight:
            Deprecated, use `regularization_weight` instead. Takes precendence over
            `regularization_weight` if not None. Weight factor lambda by which the mean
            entropy maximization regularization loss is scaled. Set to 0 to disable
            mean entropy maximization reguliarization.

        gather_distributed:
            If True, then target probabilities are gathered from all GPUs.

     Examples:

        >>> # initialize loss function
        >>> loss_fn = MSNLoss()
        >>>
        >>> # generate anchors and targets of images
        >>> anchors = transforms(images)
        >>> targets = transforms(images)
        >>>
        >>> # feed through MSN model
        >>> anchors_out = model(anchors)
        >>> targets_out = model.target(targets)
        >>>
        >>> # calculate loss
        >>> loss = loss_fn(anchors_out, targets_out, prototypes=model.prototypes)

    """

    def __init__(
        self,
        num_prototypes: int,
        prototypes_dim: int,
        temperature: float = 0.1,
        sinkhorn_iterations: int = 3,
        regularization_weight: float = 1.0,
        freeze_prototypes_niters: int = None,
        gather_distributed: bool = False,
    ):
        super().__init__()
        if temperature <= 0:
            raise ValueError(
                f"temperature must be in (0, inf) but is {temperature}.")
        if sinkhorn_iterations < 0:
            raise ValueError(
                f"sinkhorn_iterations must be >= 0 but is {sinkhorn_iterations}."
            )
        if gather_distributed and not dist.is_available():
            raise ValueError(
                "gather_distributed is True but torch.distributed is not available. "
                "Please set gather_distributed=False or install a torch version with "
                "distributed support."
            )

        self.temperature = temperature
        self.sinkhorn_iterations = sinkhorn_iterations
        self.regularization_weight = regularization_weight
        self.freeze_prototypes_niters = freeze_prototypes_niters
        self.gather_distributed = gather_distributed
        self.prototypes = nn.Linear(
            num_prototypes, prototypes_dim, bias=False).weight
        self.loss_names = ['loss', 'p_loss', 'max_entr_loss']

    @staticmethod
    def sharpen(probabilities, temperature):
        """Sharpens the probabilities with the given temperature.

        Args:
            probabilities:
                Tensor with shape (batch_size, dim)
            temperature:
                Temperature in (0, 1]. Lower temperature results in stronger sharpening (
                output probabilities are less uniform).
        Returns:
            Probabilities tensor with shape (batch_size, dim).

        """
        probabilities = probabilities ** (1.0 / temperature)
        probabilities /= torch.sum(probabilities, dim=1, keepdim=True)
        return probabilities

    @staticmethod
    def prototype_probabilities(
            queries,
            prototypes,
            temperature: float):
        """Returns probability for each query to belong to each prototype.

        Args:
            queries:
                Tensor with shape (batch_size, dim)
            prototypes:
                Tensor with shape (num_prototypes, dim)
            temperature:
                Inverse scaling factor for the similarity.

        Returns:
            Probability tensor with shape (batch_size, num_prototypes) which sums to 1 along
            the num_prototypes dimension.

        """
        return F.softmax(torch.matmul(queries, prototypes) / temperature, dim=1)

    def forward(
            self,
            anchors,
            targets,
            target_sharpen_temperature: float = 0.25,
            global_step: int = None):
        """Computes the MSN loss for a set of anchors, targets and prototypes.

        Args:
            anchors:
                Tensor with shape (batch_size * anchor_views, dim).
            targets:
                Tensor with shape (batch_size, dim).
            prototypes:
                Tensor with shape (num_prototypes, dim).
            target_sharpen_temperature:
                Temperature used to sharpen the target probabilities.

        Returns:
            Mean loss over all anchors.

        """
        self.freeze_prototypes(global_step=global_step)

        num_views = anchors.shape[0] // targets.shape[0]
        anchors = F.normalize(anchors, dim=1)
        targets = F.normalize(targets, dim=1)

        prototypes = F.normalize(self.prototypes, dim=0)

        # anchor predictions
        anchor_probs = MSNLoss.prototype_probabilities(
            anchors, prototypes, temperature=self.temperature
        )

        # target predictions
        with torch.no_grad():
            target_probs = MSNLoss.prototype_probabilities(
                targets, prototypes, temperature=self.temperature
            )
            target_probs = MSNLoss.sharpen(
                target_probs, temperature=target_sharpen_temperature)
            if self.sinkhorn_iterations > 0:
                target_probs = self.sinkhorn(
                    probabilities=target_probs,
                    iterations=self.sinkhorn_iterations,
                    use_dist=self.gather_distributed,
                )
            target_probs = target_probs.repeat((num_views, 1))

        # cross entropy loss
        p_loss = torch.mean(
            torch.sum(torch.log(anchor_probs ** (-target_probs)), dim=1))

        # regularization loss
        if self.regularization_weight > 0:
            mean_anchor_probs = torch.mean(anchor_probs, dim=0)
            reg_loss = self.regularization_weight * self.regularization_loss(
                mean_anchor_probs=mean_anchor_probs)
        else:
            reg_loss = 0
        loss = p_loss + reg_loss

        return {
            'loss': loss,
            'p_loss': p_loss,
            'max_entr_loss': reg_loss,
        }

    def regularization_loss(self, mean_anchor_probs):
        """Calculates mean entropy regularization loss."""
        loss = -torch.sum(torch.log(mean_anchor_probs ** (-mean_anchor_probs)))
        loss += math.log(float(len(mean_anchor_probs)))
        return loss

    @torch.no_grad()
    def sinkhorn(
            self,
            probabilities,
            iterations: int = 3,
            use_dist: bool = False):
        """Runs sinkhorn normalization on the probabilities as described in [0].

        Code inspired by [1].

        - [0]: Masked Siamese Networks, 2022, https://arxiv.org/abs/2204.07141
        - [1]: https://github.com/facebookresearch/msn

        Args:
            probabilities:
                Probabilities tensor with shape (batch_size, num_prototypes).
            iterations:
                Number of iterations of the sinkhorn algorithms. Set to 0 to disable.
            use_dist:
                If True then features from all gpus are gathered during normalization.
        Returns:
            A normalized probabilities tensor.

        """
        if iterations <= 0:
            return probabilities

        world_size = 1
        _got_dist = use_dist and torch.distributed.is_available() \
            and torch.distributed.is_initialized() \
            and (torch.distributed.get_world_size() > 1)
        if _got_dist:
            world_size = torch.distributed.get_world_size()

        num_targets, num_prototypes = probabilities.shape
        probabilities = probabilities.T
        sum_probabilities = torch.sum(probabilities)
        if world_size > 1:
            dist.all_reduce(sum_probabilities)
        probabilities = probabilities / sum_probabilities

        for _ in range(iterations):
            # normalize rows
            row_sum = torch.sum(probabilities, dim=1, keepdim=True)
            if world_size > 1:
                dist.all_reduce(row_sum)
            probabilities /= row_sum
            probabilities /= num_prototypes

            # normalize columns
            probabilities /= torch.sum(probabilities, dim=0, keepdim=True)
            probabilities /= num_targets

        probabilities *= num_targets
        return probabilities.T

    def freeze_prototypes(self, global_step):
        if self.freeze_prototypes_niters is not None:
            proto_requires_grad = global_step >= self.freeze_prototypes_niters
        else:
            proto_requires_grad = True
        self.prototypes.requires_grad_(proto_requires_grad)
