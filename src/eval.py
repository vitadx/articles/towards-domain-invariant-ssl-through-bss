import os
import yaml
import json
import pprint
import argparse
from typing import Optional

from prodict import Prodict
import torch
from torch import nn
from torch.optim import lr_scheduler
from torch.nn.parallel import DistributedDataParallel
from torch.utils.data import DataLoader, DistributedSampler, random_split
from torchvision import transforms

from .data import datasets
from .data.dataloaders import InfiniteDataLoader
from .modules import ssl
from .modules.augmentations.pipeline import AugmentationPipeline
from .optim import optimizers, utils
from .trainers.finetune import Trainer


def main(
        eval_yaml_config: Prodict,
        checkpoint_path: str,
        dataset_name: str,
        data_dir: str,
        log_dir: str,
        result_path: str,
        sources: list[str],
        targets: list[str],
        linear_prob: bool,
        train_size: Optional[float] = None,
        seed: int = 0):
    """Main script to evaluate a pretrained SSL model 

    Args:
        eval_yaml_config (Prodict): YAML eval config path
        checkpoint_path (str): Model checkpoint path
        dataset_name (str): Name of the dataset in 
            ['Camelyon17WILDS', 'PACS', 'DomainNetSubset']
        data_dir (str): Directory of the different datasets: '
                        'This directory should contains camelyon17, PACS, '
                        'domainnet_subset directories.
        log_dir (str): Directory for tensorboard logs
        result_path (str): JSON results file path
        sources (list[str]): Source domains
        targets (list[str]): Target domains
        linear_prob (bool): Whether or not to use linear probing. 
            Default value is False
        train_size (float, optional): Ratio of the train data to consider for finetuning. 
            Defaults to None.
        seed (int, optional): Seed used for sampling a fraction of train data. 
            Defaults to 0.
    """

    # Retrieve environment variables
    use_slurm = 'SLURM_JOB_NAME' in os.environ
    job_name = os.environ['SLURM_JOB_NAME'] if use_slurm else 'local_job'
    host_node_addr = os.environ['HOST_NODE_ADDR'] if use_slurm else 'localhost'
    master_addr = os.environ['MASTER_ADDR']
    master_port = os.environ['MASTER_PORT']
    world_size = os.environ['WORLD_SIZE']
    rank = os.environ['RANK']
    local_rank = os.environ['LOCAL_RANK']
    env_variables = {
        'job_name': job_name,
        'host_node_addr': host_node_addr,
        'master_addr': master_addr,
        'master_port': master_port,
        'world_size': world_size,
        'rank': rank,
        'local_rank': local_rank,
    }
    print('ENVIRONMENT VARIABLES'.center(80, '-'))
    pprint.pprint(env_variables)
    print('-'*80+'\n')

    # Load ssl checkpoint and load config
    chkpt = torch.load(checkpoint_path)
    ssl_config = Prodict.from_dict(chkpt['config'])
    print('SSL CONFIG'.center(80, '-'))
    pprint.pprint(ssl_config)
    print('-'*80+'\n')

    # Load eval yaml config
    with open(file=eval_yaml_config, mode='r') as f:
        eval_config = Prodict.from_dict(yaml.safe_load(f))
    print('EVAL CONFIG'.center(80, '-'))
    pprint.pprint(eval_config)
    print('-'*80+'\n')

    # Initialize process group
    torch.distributed.init_process_group(
        backend="nccl",
        rank=int(rank),
        world_size=int(world_size))
    local_rank = int(local_rank)
    torch.cuda.set_device(device=local_rank)
    device = torch.device("cuda")

    # Define transforms
    train_transform = transforms.Compose([
        transforms.Resize(
            ssl_config.TRAIN.TRANSFORMS.RESIZED_SHAPE,
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.ToTensor()
    ])
    test_transform = transforms.Compose([
        transforms.Resize(
            ssl_config.TRAIN.TRANSFORMS.RESIZED_SHAPE,
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.ToTensor()
    ])

    # Get datasets
    dsets = {}
    for split_name in ['train', 'id_val', 'test']:
        is_train = split_name == 'train'
        is_id_val = split_name == 'id_val'
        dataset = getattr(datasets, dataset_name)(
            data_dir=data_dir,
            split_name=split_name,
            transform=train_transform if is_train else test_transform,
            domains=sources if is_train or is_id_val else targets)
        if is_train:
            num_classes = len(dataset.labels_count)
            if train_size is not None:
                dataset, *_ = random_split(
                    dataset=dataset,
                    lengths=[train_size, 1-train_size],
                    generator=torch.Generator().manual_seed(seed))

        dsets[split_name] = dataset

    print('DATASETS'.center(80, '-'))
    pprint.pprint(dsets)
    print('-'*80+'\n')

    # Get dataloaders
    dataloaders = {}
    for dataset_name, dataset in dsets.items():
        is_train = dataset_name == 'train'
        cls_dataloader = InfiniteDataLoader if \
            is_train and eval_config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH is not None else\
            DataLoader
        sampler = DistributedSampler(
            dataset=dataset,
            drop_last=is_train,
            shuffle=True)
        dataloaders[dataset_name] = cls_dataloader(
            dataset=dsets[dataset_name],
            batch_size=eval_config.TRAIN.OPTIMIZATION.BATCH_SIZE,
            num_workers=8,
            sampler=sampler,
            drop_last=is_train,
            pin_memory=True)

    # Create ssl model + retrieve backbone model
    model = ssl.get_ssl_model(ssl_config).to(device)
    model = DistributedDataParallel(
        model, device_ids=[local_rank], output_device=local_rank,
        broadcast_buffers=False,
        find_unused_parameters=True
    )
    model.load_state_dict(chkpt['networks']['model'])
    backbone = model.module.backbone

    # For linear probing or finetuning, freeze/unfreeze weights +
    # create the right classification head
    if linear_prob:
        backbone = backbone.eval()
        for p in backbone.parameters():
            p.requires_grad = False
        backbone.fc = nn.Sequential(
            nn.BatchNorm1d(num_features=backbone.out_features, affine=False),
            nn.Linear(
                backbone.out_features, num_classes)).to(device).train()
    else:
        backbone.fc = nn.Linear(
            backbone.out_features, num_classes).to(device).train()
    backbone = DistributedDataParallel(
        module=backbone, device_ids=[local_rank], output_device=local_rank,
        broadcast_buffers=False)

    nets = {
        'model': backbone,
    }
    print('NETWORKS'.center(80, '-'))
    pprint.pprint(nets)
    for net_name, net in nets.items():
        print(
            f'{net_name}: {sum(p.numel() for p in net.parameters() if p.requires_grad):,}')
    print('-'*80+'\n')

    # Define augmentation pipeline based on the yaml config
    augmentation_pipeline = AugmentationPipeline(config=eval_config).to(device)

    # Create optimizers and learning rate schedule
    params = utils.get_params_and_excluded_params_for_weight_decay(
        model,
        weight_decay=eval_config.TRAIN.OPTIMIZATION.WEIGHT_DECAY)
    opts = {'model': optimizers.get_optimizer(
        params=params, config=eval_config)}
    lr_schs = {
        'model': lr_scheduler.CosineAnnealingLR(
            optimizer=opts['model'],
            T_max=eval_config.TRAIN.OPTIMIZATION.EPOCHS,
            eta_min=0)
    }

    # Create trainer and train the model
    trainer = Trainer()
    trainer.train(
        networks=nets,
        dataloaders=dataloaders,
        augmentation_pipeline=augmentation_pipeline,
        optimizers=opts,
        lr_schedulers=lr_schs,
        config=eval_config,
        log_dir=log_dir,
        device=device,
    )

    eval_metrics = trainer.evaluate(
        networks=nets,
        dataloader=dataloaders['test'],
        config=eval_config,
        device=device,
    )

    os.makedirs(os.path.dirname(result_path), exist_ok=True)
    with open(file=result_path, mode='w') as f:
        json.dump(eval_metrics, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--eval_yaml_config", type=str,
                        help='YAML eval config path')
    parser.add_argument("--checkpoint_path", type=str,
                        help='Model checkpoint path')
    parser.add_argument("--dataset_name", type=str, choices=[
                        'Camelyon17WILDS', 'PACS', 'DomainNetSubset'],
                        help='Name of the dataset')
    parser.add_argument("--data_dir", type=str,
                        help='Directory of the different datasets: '
                        'This directory should contains camelyon17, PACS, '
                        'domainnet_subset directories.')
    parser.add_argument("--log_dir", type=str,
                        help='Directory for tensorboard logs')
    parser.add_argument("--result_path", type=str,
                        help='JSON results file path')
    parser.add_argument("--sources", nargs='+', type=str,
                        help='Source domains')
    parser.add_argument("--targets", nargs='+', type=str,
                        help='Target domains')
    parser.add_argument("--linear_prob", action='store_true',
                        help='Whether or not to use linear probing. Default value is False')
    parser.set_defaults(linear_prob=False)
    parser.add_argument("--train_size", type=float,
                        help='Ratio of the train data to consider for finetuning',
                        default=None)
    parser.add_argument("--seed", type=int,
                        help='Seed used for sampling a fraction of train data',
                        default=0)
    args = parser.parse_args()
    main(
        eval_yaml_config=args.eval_yaml_config,
        checkpoint_path=args.checkpoint_path,
        dataset_name=args.dataset_name,
        data_dir=args.data_dir,
        log_dir=args.log_dir,
        result_path=args.result_path,
        sources=args.sources,
        targets=args.targets,
        linear_prob=args.linear_prob,
        train_size=args.train_size,
        seed=args.seed,
    )
