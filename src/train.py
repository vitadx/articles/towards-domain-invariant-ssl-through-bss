import os
import yaml
import pprint
import argparse
from typing import Union

from prodict import Prodict
import torch
from torch.nn.parallel import DistributedDataParallel
from torch.utils.data import DataLoader, DistributedSampler
from torchvision import transforms

from .data import datasets
from .data.dataloaders import InfiniteDataLoader
from .modules import ssl
from .modules.augmentations.pipeline import AugmentationPipeline
from .optim import optimizers, lr_schedulers, utils
from .trainers.ssl import Trainer


def main(
        yaml_config: Prodict,
        dataset_name: str,
        data_dir: str,
        log_dir: str,
        checkpoint_path: str,
        sources: Union[None, list[str]],
        targets: Union[None, list[str]]):
    """Main script to train a SSL method 

    Args:
        yaml_config (Prodict): YAML config path for the SSL method training 
        dataset_name (str): Name of the dataset. It should be included in 
            ['Camelyon17WILDS', 'PACS', 'DomainNetSubset']
        data_dir (str): Directory of the different datasets: '
                        'This directory should contains camelyon17, PACS, '
                        'domainnet_subset directories.
        log_dir (str): Directory for tensorboard logs
        checkpoint_path (str): Model checkpoint path
        sources (Optional[list[str]], optional): Source domains
        targets (Optional[list[str]], optional): Target domains
    """

    # Retrieve environment variables
    use_slurm = 'SLURM_JOB_NAME' in os.environ
    job_name = os.environ['SLURM_JOB_NAME'] if use_slurm else 'local_job'
    host_node_addr = os.environ['HOST_NODE_ADDR'] if use_slurm else 'localhost'
    master_addr = os.environ['MASTER_ADDR']
    master_port = os.environ['MASTER_PORT']
    world_size = os.environ['WORLD_SIZE']
    rank = os.environ['RANK']
    local_rank = os.environ['LOCAL_RANK']
    env_variables = {
        'job_name': job_name,
        'host_node_addr': host_node_addr,
        'master_addr': master_addr,
        'master_port': master_port,
        'world_size': world_size,
        'rank': rank,
        'local_rank': local_rank,
    }
    print('ENVIRONMENT VARIABLES'.center(80, '-'))
    pprint.pprint(env_variables)
    print('-'*80+'\n')

    # Load yaml config
    with open(file=yaml_config, mode='r') as f:
        config = Prodict.from_dict(yaml.safe_load(f))
    print('CONFIG'.center(80, '-'))
    pprint.pprint(config)
    print('-'*80+'\n')

    # Initialize process group
    torch.distributed.init_process_group(
        backend="nccl",
        rank=int(rank),
        world_size=int(world_size))
    local_rank = int(local_rank)
    torch.cuda.set_device(device=local_rank)
    device = torch.device("cuda")

    # Define transforms
    train_transform = transforms.Compose([
        transforms.Resize(
            config.TRAIN.TRANSFORMS.RESIZED_SHAPE,
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.ToTensor()
    ])
    test_transform = transforms.Compose([
        transforms.Resize(
            config.TRAIN.TRANSFORMS.RESIZED_SHAPE,
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.ToTensor()
    ])

    # Get datasets
    dsets = {}
    for split_name in ['train', 'id_val', 'test']:
        is_train = split_name == 'train'
        is_id_val = split_name == 'id_val'
        dsets[split_name] = getattr(datasets, dataset_name)(
            data_dir=data_dir,
            split_name=split_name,
            transform=train_transform if is_train else test_transform,
            domains=sources if is_train or is_id_val else targets)
    print('DATASETS'.center(80, '-'))
    pprint.pprint(dsets)
    print('-'*80+'\n')

    # Get dataloaders
    dataloaders = {}
    for dataset_name, dataset in dsets.items():
        is_train = dataset_name == 'train'
        cls_dataloader = InfiniteDataLoader if \
            is_train and config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH is not None else\
            DataLoader
        sampler = DistributedSampler(
            dataset=dataset,
            drop_last=is_train,
            shuffle=True)
        dataloaders[dataset_name] = cls_dataloader(
            dataset=dsets[dataset_name],
            batch_size=config.TRAIN.OPTIMIZATION.BATCH_SIZE,
            num_workers=8,
            sampler=sampler,
            drop_last=is_train,
            pin_memory=True)

    # Create model
    model = ssl.get_ssl_model(config)
    model = model.train().to(device)
    model = DistributedDataParallel(
        model, device_ids=[local_rank], output_device=local_rank,
        broadcast_buffers=False,
        find_unused_parameters=True
    )
    nets = {
        'model': model,
    }
    print('NETWORKS'.center(80, '-'))
    pprint.pprint(nets)
    for net_name, net in nets.items():
        print(
            f'{net_name}: {sum(p.numel() for p in net.parameters() if p.requires_grad):,}')
    print('-'*80+'\n')

    # Define augmentation pipeline based on the yaml config
    augmentation_pipeline = AugmentationPipeline(config=config).to(device)

    # Create optimizers and learning rate schedule
    params = utils.get_params_and_excluded_params_for_weight_decay(
        model,
        weight_decay=config.TRAIN.OPTIMIZATION.WEIGHT_DECAY)
    opts = {'model': optimizers.get_optimizer(params=params, config=config)}
    lr_schs = {
        'model': lr_schedulers.LinearWarmupCosineAnnealingLR(
            optimizer=opts['model'],
            warmup_epochs=config.TRAIN.OPTIMIZATION.WARMUP_EPOCHS,
            max_epochs=config.TRAIN.OPTIMIZATION.EPOCHS,
            warmup_start_lr=config.TRAIN.OPTIMIZATION.MIN_LR,
            eta_min=config.TRAIN.OPTIMIZATION.MIN_LR)
    }

    # Load checkpoint if checkpoint_path exists
    os.makedirs(os.path.dirname(checkpoint_path), exist_ok=True)
    prev_checkpoint_exists = os.path.exists(checkpoint_path)
    if prev_checkpoint_exists:
        print('Checkpoint loading...')
        Trainer.load_checkpoint(
            checkpoint_path=checkpoint_path,
            networks=nets,
            optimizers=opts,
            lr_schedulers=lr_schs,
        )
        print('Checkpoint loaded!')

    # Create trainer and train the model
    os.makedirs(log_dir, exist_ok=True)
    trainer = Trainer()
    trainer.train(
        networks=nets,
        dataloaders=dataloaders,
        augmentation_pipeline=augmentation_pipeline,
        optimizers=opts,
        lr_schedulers=lr_schs,
        config=config,
        log_dir=log_dir,
        checkpoint_path=checkpoint_path,
        device=device,
    )

    # Dump the model, optimizer, scheduler and config
    Trainer.dump_checkpoint(
        checkpoint_path=checkpoint_path,
        networks=nets,
        optimizers=opts,
        lr_schedulers=lr_schs,
        config=config,
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--yaml_config", type=str,
                        help='YAML config path')
    parser.add_argument("--dataset_name", type=str, choices=[
                        'Camelyon17WILDS', 'PACS', 'DomainNetSubset'],
                        help='Name of the dataset')
    parser.add_argument("--data_dir", type=str,
                        help='Directory of the different datasets: '
                        'This directory should contains camelyon17, PACS, '
                        'domainnet_subset directories.')
    parser.add_argument("--log_dir", type=str,
                        help='Directory for tensorboard logs')
    parser.add_argument("--checkpoint_path", type=str,
                        help='Model checkpoint path')
    parser.add_argument("--sources", nargs='+', type=str,
                        help='Source domains')
    parser.add_argument("--targets", nargs='+', type=str,
                        help='Target domains')
    args = parser.parse_args()
    main(
        yaml_config=args.yaml_config,
        dataset_name=args.dataset_name,
        data_dir=args.data_dir,
        log_dir=args.log_dir,
        checkpoint_path=args.checkpoint_path,
        sources=args.sources,
        targets=args.targets)
