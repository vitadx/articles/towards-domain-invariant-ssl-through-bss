import os
import tqdm

from prodict import Prodict

import torch
from torch.cuda.amp import GradScaler
from torch import nn
from torch.utils.data import DataLoader
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LRScheduler
from torch.utils.tensorboard import SummaryWriter
from torchmetrics.aggregation import MeanMetric

from ..modules.augmentations.pipeline import AugmentationPipeline


class Trainer():

    @staticmethod
    def initialize_running_metrics(losses_names, device):
        return {loss_name: MeanMetric().to(device) for loss_name in losses_names}

    def train(
            self,
            networks: dict[str, nn.Module],
            dataloaders: dict[str, DataLoader],
            augmentation_pipeline: AugmentationPipeline,
            optimizers: dict[str, Optimizer],
            lr_schedulers: dict[str, LRScheduler],
            config: Prodict,
            log_dir: str,
            checkpoint_path: str,
            device: torch.device):
        """SSL method Trainer

        Args:
            networks (dict[str, nn.Module]): Dictionary containing the different 
                networks to train,
            dataloaders (dict[str, DataLoader]): Dataloaders 
                (train, id_val, test).
            augmentation_pipeline (AugmentationPipeline): Augmentation pipeline
                to construct from a single batch multiple augmented batches 
                containing several views.
            optimizers (dict[str, Optimizer]): Optimizer for each network
            lr_schedulers (dict[str, LRScheduler]): Learning rate scheduler 
                for each optimizer.
            config (Prodict): Configuration of the SSL method
            log_dir (str): Directory for tensorboard logs
            checkpoint_path (str): Model checkpoint path
            device (torch.device): Torch device
        """
        rank = int(os.environ.get('RANK', 0))
        writer = SummaryWriter(log_dir)

        grad_scaler = GradScaler(
            enabled=config.TRAIN.OPTIMIZATION.USE_MIXED_PRECISION)

        steps_per_epoch = len(dataloaders['train']) if config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH is None\
            else config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH
        global_step = int(
            lr_schedulers['model'].last_epoch * steps_per_epoch)

        for epoch in range(lr_schedulers['model'].last_epoch, config.TRAIN.OPTIMIZATION.EPOCHS):
            running_metrics = Trainer.initialize_running_metrics(
                losses_names=networks['model'].module.loss_names,
                device=device)
            dataloaders['train'].sampler.set_epoch(epoch)

            pbar = tqdm.tqdm(
                dataloaders['train'],
                total=steps_per_epoch,
                desc=f'{epoch+1}/{config.TRAIN.OPTIMIZATION.EPOCHS} Epochs |')

            for batch_index, batch in enumerate(pbar, start=1):
                _, losses = self.train_on_batch(
                    networks=networks,
                    batch=batch,
                    augmentation_pipeline=augmentation_pipeline,
                    optimizers=optimizers,
                    grad_scaler=grad_scaler,
                    config=config,
                    global_step=global_step,
                    device=device,
                )
                global_step += 1

                # Update metrics values
                for loss_name, loss in losses.items():
                    running_metrics[loss_name].update(loss)

                lr = lr_schedulers['model'].get_last_lr()[0]
                str_metrics = ', '.join(
                    [f'{metric_name}: {metric.compute():.4f}' for metric_name, metric in running_metrics.items()])
                desc = f'{epoch+1}/{config.TRAIN.OPTIMIZATION.EPOCHS} Epochs | lr: {lr:.4f}, {str_metrics}'
                pbar.set_description(desc)

                if batch_index == steps_per_epoch:
                    break

            if rank == 0 and (epoch+1) % config.TRAIN.CHECKPOINT_FREQUENCY == 0:
                print("Dumping model...")
                Trainer.dump_checkpoint(
                    checkpoint_path=checkpoint_path,
                    networks=networks,
                    optimizers=optimizers,
                    lr_schedulers=lr_schedulers,
                    config=config,
                )

            # Write the learning rate, losses and acc
            for metric_name, metric in running_metrics.items():
                writer.add_scalar(
                    f'train/{metric_name}',
                    metric.compute(),
                    global_step)

            writer.add_scalar(
                "train/lr", lr,
                global_step=global_step)

            # Update the learning rate
            lr_schedulers['model'].step()

    def train_on_batch(
            self,
            networks: dict[str, nn.Module],
            batch: dict[str, torch.Tensor],
            augmentation_pipeline: AugmentationPipeline,
            optimizers: dict[str, Optimizer],
            grad_scaler: GradScaler,
            config: Prodict,
            global_step: int,
            device: torch.device):
        for opt_name in optimizers:
            optimizers[opt_name].zero_grad(set_to_none=True)

        with torch.cuda.amp.autocast(
                dtype=torch.float16,
                enabled=config.TRAIN.OPTIMIZATION.USE_MIXED_PRECISION):
            imgs = batch['imgs'].to(device)
            views = augmentation_pipeline(imgs)
            outputs, losses = networks['model'](
                views=views,
                global_step=global_step)
        grad_scaler.scale(losses['loss']).backward()
        grad_scaler.step(optimizers['model'])
        grad_scaler.update()
        return outputs, losses

    @staticmethod
    def dump_checkpoint(
            checkpoint_path: str,
            networks: dict[str, nn.Module],
            optimizers: dict[str, Optimizer],
            lr_schedulers: dict[str, LRScheduler],
            config: Prodict):
        torch.save({
            'networks': {
                network_name: network.state_dict() for network_name,
                network in networks.items()},
            'optimizers': {
                optimizer_name: optimizer.state_dict() for optimizer_name,
                optimizer in optimizers.items()},
            'lr_schedulers': {
                scheduler_name: scheduler.state_dict() for scheduler_name,
                scheduler in lr_schedulers.items()},
            'config': config},
            checkpoint_path)

    @staticmethod
    def load_checkpoint(
            checkpoint_path: str,
            networks: dict[str, nn.Module],
            optimizers: dict[str, Optimizer],
            lr_schedulers: dict[str, LRScheduler]):
        chkpt = torch.load(checkpoint_path)
        for network_name, state_dict in chkpt['networks'].items():
            networks[network_name].load_state_dict(state_dict)
        for opt_name, state_dict in chkpt['optimizers'].items():
            optimizers[opt_name].load_state_dict(state_dict)
        for sched_name, state_dict in chkpt['lr_schedulers'].items():
            lr_schedulers[sched_name].load_state_dict(state_dict)
