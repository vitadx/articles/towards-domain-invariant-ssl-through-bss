import tqdm
import numpy as np

from prodict import Prodict

import torch
from torch.cuda.amp import GradScaler
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LRScheduler
from torch.utils.tensorboard import SummaryWriter
from torchmetrics.aggregation import MeanMetric

from ..modules.augmentations.pipeline import AugmentationPipeline


class Trainer():

    @staticmethod
    def initialize_running_metrics(device):
        return {'loss': MeanMetric().to(device)}

    def train(
            self,
            networks: dict[str, nn.Module],
            dataloaders: dict[str, DataLoader],
            augmentation_pipeline: AugmentationPipeline,
            optimizers: dict[str, Optimizer],
            lr_schedulers: dict[str, LRScheduler],
            config: Prodict,
            log_dir: str,
            device: torch.device):
        """Finetune SSL method

        Args:
            networks (dict[str, nn.Module]): Dictionary containing the different 
                networks to train,
            dataloaders (dict[str, DataLoader]): Dataloaders 
                (train, id_val, test).
            augmentation_pipeline (AugmentationPipeline): Augmentation pipeline
                to construct from a single batch multiple augmented batches 
                containing several views.
            optimizers (dict[str, Optimizer]): Optimizer for each network
            lr_schedulers (dict[str, LRScheduler]): Learning rate scheduler 
                for each optimizer.
            config (Prodict): Configuration of the SSL method
            log_dir (str): Directory for tensorboard logs
            device (torch.device): Torch device
        """
        writer = SummaryWriter(log_dir)

        grad_scaler = GradScaler(
            enabled=config.TRAIN.OPTIMIZATION.USE_MIXED_PRECISION)

        steps_per_epoch = len(dataloaders['train']) if config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH is None\
            else config.TRAIN.OPTIMIZATION.STEPS_PER_EPOCH
        global_step = int(
            lr_schedulers['model'].last_epoch * steps_per_epoch)

        for epoch in range(lr_schedulers['model'].last_epoch, config.TRAIN.OPTIMIZATION.EPOCHS):
            running_metrics = Trainer.initialize_running_metrics(device=device)
            dataloaders['train'].sampler.set_epoch(epoch)

            pbar = tqdm.tqdm(
                dataloaders['train'],
                total=steps_per_epoch,
                desc=f'{epoch+1}/{config.TRAIN.OPTIMIZATION.EPOCHS} Epochs |')

            for batch_index, batch in enumerate(pbar, start=1):
                _, losses = self.train_on_batch(
                    networks=networks,
                    batch=batch,
                    augmentation_pipeline=augmentation_pipeline,
                    optimizers=optimizers,
                    grad_scaler=grad_scaler,
                    config=config,
                    device=device,
                )
                global_step += 1

                # Update metrics values
                for loss_name, loss in losses.items():
                    running_metrics[loss_name].update(loss)

                lr = lr_schedulers['model'].get_last_lr()[0]
                str_metrics = ', '.join(
                    [f'{metric_name}: {metric.compute():.4f}' for metric_name, metric in running_metrics.items()])
                desc = f'{epoch+1}/{config.TRAIN.OPTIMIZATION.EPOCHS} Epochs | lr: {lr:.4f}, {str_metrics}'
                pbar.set_description(desc)

                if batch_index == steps_per_epoch:
                    break

            # Write the learning rate, losses and acc
            for metric_name, metric in running_metrics.items():
                writer.add_scalar(
                    f'train/{metric_name}',
                    metric.compute(),
                    global_step)

            writer.add_scalar(
                "train/lr", lr,
                global_step=global_step)

            # Update the learning rate
            lr_schedulers['model'].step()

            # self.evaluate(
            #     networks=networks,
            #     dataloader=dataloaders['test'],
            #     config=config,
            #     device=device,
            # )

    def train_on_batch(
            self,
            networks: dict[str, nn.Module],
            batch: dict[str, torch.Tensor],
            augmentation_pipeline: AugmentationPipeline,
            optimizers: dict[str, Optimizer],
            grad_scaler: GradScaler,
            config: Prodict,
            device: torch.device):
        for opt_name in optimizers:
            optimizers[opt_name].zero_grad(set_to_none=True)

        with torch.cuda.amp.autocast(
                dtype=torch.float16,
                enabled=config.TRAIN.OPTIMIZATION.USE_MIXED_PRECISION):
            imgs = batch['imgs'].to(device)
            views = augmentation_pipeline(imgs)
            if isinstance(views, list):
                if len(views) == 1:
                    views = views[0]
            y_pred = networks['model'](views)
            y_true = batch['label'].to(device)
            outputs = {
                'y_pred': y_pred
            }
            losses = {
                'loss': F.cross_entropy(input=y_pred, target=y_true)
            }

        grad_scaler.scale(losses['loss']).backward()
        grad_scaler.step(optimizers['model'])
        grad_scaler.update()
        return outputs, losses

    @torch.inference_mode()
    def evaluate(
            self,
            networks: dict[str, nn.Module],
            dataloader: DataLoader,
            config: Prodict,
            device: torch.device):
        """Evaluate the model on a given dataloader

        Args:
            networks (dict[str, nn.Module]): Dictionary containing the different 
                networks to train,
            dataloader (DataLoader): Dataloader 
                (train, id_val, test).
            config (Prodict): Configuration of the SSL method
            device (torch.device): Torch device
        """
        networks['model'].eval()
        pbar = tqdm.tqdm(
            dataloader,
            total=len(dataloader),
            desc='Evaluation |')
        eval_metrics = dataloader.dataset.initialize_eval_metrics(
            device=device)
        for batch in pbar:
            with torch.cuda.amp.autocast(
                    dtype=torch.float16,
                    enabled=config.TRAIN.OPTIMIZATION.USE_MIXED_PRECISION):
                imgs = batch['imgs'].to(device)
                y_pred = networks['model'](imgs)
                y_true = batch['label'].to(device)
                domains = np.array(batch['domain'])

                eval_metrics['overall'].update(y_pred, y_true)
                for domain in np.unique(domains):
                    in_domain = torch.from_numpy(domains == domain)
                    eval_metrics[domain].update(
                        y_pred[in_domain], y_true[in_domain])

        # Convert metric tensor to float value to be able to dump
        results = {}
        for domain in eval_metrics:
            if domain not in results:
                results[domain] = {}
            for metric_name in eval_metrics[domain]:
                results[domain][metric_name] = eval_metrics[domain][metric_name].compute(
                ).item()
        networks['model'].eval()
        return results
