# Towards domain-invariant Self-Supervised Learning with Batch Styles Standardization

<center>
	<h2>| <a href="https://openreview.net/pdf?id=qtE9K23ISq">Paper</a> |</h2>
</center>

This repository contains the official pytorch implementations of the Batch Styles Standardization and extended Self-supervised Learning methods presented in [Towards domain-invariant Self-Supervised Learning with Batch Styles Standardization (ICLR 2024)](https://openreview.net/pdf?id=qtE9K23ISq).

<figure align="center">
<img src="./imgs/BSS.gif" width="50%" align="center" alt="Batch Styles Standardization">
 <figcaption style='text-align: center;'><b></b></figcaption>
</figure>

<!-- ## :warning: Project Status

The code is still being developed and should be functional soon. -->

## :wrench: 1. Requirements

### Clone the repository and change directory
```bash
git clone https://gitlab.com/vitadx/articles/towards-domain-invariant-ssl-through-bss.git
cd towards-domain-invariant-ssl-through-bss
```

### Setup environment via Conda

If you are using conda to manage your python environments, you need first to create a conda environment and install the different dependencies specified in `towards_domain_invariant_ssl_through_bss.yml` via the following command:
```bash
conda env create \
--name towards-domain-invariant-ssl-through-bss \
--file envs/towards-domain-invariant-ssl-through-bss.yml
```
and then activate the environment:
```bash
conda activate towards-domain-invariant-ssl-through-bss
```

### How to download and organize the datasets directory?

#### Download the datasets

Camelyon17 WILDS can be downloaded throught the package `wilds` while DomainNet and PACS can be downloaded through the following URL:
| Dataset       | Link                                                                                                      |
|:--------------|:----------------------------------------------------------------------------------------------------------|
| DomainNet     | <a href="http://ai.bu.edu/M3SDA/#dataset" target="_blank">data</a>                                        |
| PACS          | <a href="https://drive.google.com/file/d/1m4X4fROCCXMO0lRLrr6Zz9Vb3974NWhE/view" target="_blank">data</a> |

#### Organize data directory

The directory containing the different datasets should be organized as the following:
```bash
data
├── camelyon17_v1.0
├── domainnet_subset
└── PACS
```

##### DomainNet subset 

```bash
domainnet_subset
├── domainnet_subset.json
├── clipart
│   ├── bee
│   ├── ...
│   └── zigzag
├── infograph
│   ├── bee
│   ├── ...
│   └── zigzag
├── painting
│   ├── bee
│   ├── ...
│   └── zigzag
├── quickdraw
│   ├── bee
│   ├── ...
│   └── zigzag
├── real
│   ├── bee
│   ├── ...
│   └── zigzag
└── sketch
    ├── bee
    ├── ...
    └── zigzag
```

##### PACS 

```bash
PACS
├── train
│   ├── art_painting
│   │   ├── dog
│   │   ├── ...
│   │   └── person
│   ├── cartoon
│   │   ├── dog
│   │   ├── ...
│   │   └── person
│   ├── photo
│   │   ├── dog
│   │   ├── ...
│   │   └── person
│   └── sketch
│       ├── dog
│       ├── ...
│       └── person
└── val
    ├── art_painting
    │   ├── dog
    │   ├── ...
    │   └── person
    ├── cartoon
    │   ├── dog
    │   ├── ...
    │   └── person
    ├── photo
    │   ├── dog
    │   ├── ...
    │   └── person
    └── sketch
        ├── dog
        ├── ...
        └── person
```

## :computer: 2. How to use Batch Styles Standardization for you own project?

```python
from matplotlib import pyplot as plt

from wilds import get_dataset
from wilds.common.data_loaders import get_train_loader
from torchvision import transforms
from torchvision.transforms import functional as TF
from torchvision.utils import make_grid

from src.modules.augmentations.fourier import BatchStylesStandardization

# my_data_dir is the directory where the dataset Camelyon17 WILDS is dumped
my_data_dir = './data'
# Load the full dataset, and download it if necessary
camelyon17  = get_dataset(
  dataset='camelyon17', 
  root_dir=my_data_dir, 
  download=True)
# Get the training set
train_data = camelyon17.get_subset("train", transform=transforms.ToTensor())
# Prepare the standard data loader
train_loader = get_train_loader("standard", train_data, batch_size=8)

# Sample batch of imgs (N, 3, H, W)
raw_imgs = next(iter(train_loader))[0]
# Apply BSS 4 times on the batch to have 4 augmented views of each image (N, 4, 3, H, W)
n_views = 4
bss_imgs = BatchStylesStandardization(areas_ratios=[0.02, 0.1])(
  imgs=raw_imgs, n_views=n_views)

# Visualize raw images and augmented images
grid_raw_imgs = make_grid(tensor=raw_imgs, nrow=1)
grid_bss_imgs = make_grid(tensor=bss_imgs.flatten(0, 1), nrow=n_views)
fig, axes = plt.subplots(1, 2, squeeze=False)
axes[0, 0].imshow(TF.to_pil_image(grid_raw_imgs))
axes[0, 0].set_title('Raw images')
axes[0, 1].imshow(TF.to_pil_image(grid_bss_imgs))
axes[0, 1].set_title('Images augmented with BSS')
plt.tight_layout()
plt.show()
```
<figure align="center">
<img src="./imgs/bss_imgs_camelyon17.png" width="50%" align="center" alt="Camelyon17 WILDS images augmented with Batch Styles Standardization">
 <figcaption style='text-align: center;'><b>Camelyon17 WILDS images augmented with Batch Styles Standardization</b></figcaption>
</figure>


## :chart_with_upwards_trend: 3. How to run experiments?

### Training 
- To train locally a Resnet-50 with SimCLR + BSS on Camelyon17 WILDS, run the following commands:

```bash
torchrun --rdzv-backend=c10d --nnodes=1 --nproc-per-node=1 \
-m towards-domain-invariant-ssl-through-bss.src.train \
--yaml_config towards-domain-invariant-ssl-through-bss/src/configs/ssl/camelyon17/simclr/simclr_multi_crops_bss.yml \
--dataset_name Camelyon17WILDS \
--data_dir towards-domain-invariant-ssl-through-bss/data \
--log_dir towards-domain-invariant-ssl-through-bss/logs/ssl/camelyon17/simclr_multi_crops_bss \
--checkpoint_path towards-domain-invariant-ssl-through-bss/checkpoints/ssl/camelyon17/simclr_multi_crops_bss.pth
```

- To train locally a Resnet-18 with SWaV + BSS on DomainNet subset with :
  * sources = $\textit{clipart} \cup \textit{infograph} \cup \textit{quickdraw}$
  * targets = $\textit{painting} \cup \textit{real} \cup \textit{sketch}$:
```bash
torchrun --rdzv-backend=c10d --nnodes=1 --nproc-per-node=1 \
-m towards-domain-invariant-ssl-through-bss.src.train \
--yaml_config towards-domain-invariant-ssl-through-bss/src/configs/ssl/domainnet_subset/swav/swav_multi_crops_bss.yml \
--dataset_name DomainNetSubset \
--data_dir towards-domain-invariant-ssl-through-bss/data \
--log_dir towards-domain-invariant-ssl-through-bss/logs/ssl/domainnet_subset/painting_real_sketch/swav_multi_crops_bss \
--checkpoint_path towards-domain-invariant-ssl-through-bss/checkpoints/ssl/domainnet_subset/painting_real_sketch/swav_multi_crops_bss.pth \
--sources clipart infograph quickdraw \
--targets painting real sketch
```

### Evaluation

Evaluate via linear probing the pretrained Resnet-50 with SimCLR + BSS on 10% of Camelyon17 WILDS:
```bash
TRAIN_SIZE=0.1

torchrun --rdzv-backend=c10d --nnodes=1 --nproc-per-node=1 \
-m towards-domain-invariant-ssl-through-bss.src.eval \
--eval_yaml_config towards-domain-invariant-ssl-through-bss/src/configs/eval/camelyon17/camelyon17_eval.yml \
--checkpoint_path towards-domain-invariant-ssl-through-bss/checkpoints/ssl/camelyon17/simclr_multi_crops_bss.pth \
--dataset_name Camelyon17WILDS \
--data_dir towards-domain-invariant-ssl-through-bss/data \
--log_dir towards-domain-invariant-ssl-through-bss/logs/eval/camelyon17/simclr_multi_crops_bss \
--result_path towards-domain-invariant-ssl-through-bss/results/camelyon17/linear_prob/$TRAIN_SIZE/simclr_multi_crops_bss.json \
--linear_prob \
--train_size $TRAIN_SIZE \
--seed 0
```

Evaluate via full finetuning the pretrained Resnet-18 with SWaV + BSS on 100% of DomainNet subset with:
* sources = $\textit{clipart} \cup \textit{infograph} \cup \textit{quickdraw}$
* targets = $\textit{painting} \cup \textit{real} \cup \textit{sketch}$:
```bash
TRAIN_SIZE=1

torchrun --rdzv-backend=c10d --nnodes=1 --nproc-per-node=1 \
-m towards-domain-invariant-ssl-through-bss.src.eval \
--eval_yaml_config towards-domain-invariant-ssl-through-bss/src/configs/eval/domainnet_subset/domainnet_subset_eval.yml \
--checkpoint_path towards-domain-invariant-ssl-through-bss/checkpoints/ssl/domainnet_subset/painting_real_sketch/swav_multi_crops_bss.pth \
--dataset_name DomainNetSubset \
--data_dir towards-domain-invariant-ssl-through-bss/data \
--log_dir towards-domain-invariant-ssl-through-bss/logs/eval/domainnet_subset/painting_real_sketch/swav_multi_crops_bss \
--result_path towards-domain-invariant-ssl-through-bss/results/domainnet_subset/painting_real_sketch/finetuning/$TRAIN_SIZE/swav_multi_crops_bss.json \
--sources clipart infograph quickdraw \
--targets painting real sketch \
--train_size $TRAIN_SIZE \
--seed 0
```

## :pencil2: 4. Citation

If you use our work for further research, please cite us:
```
@inproceedings{scalbert2023towards,
  title={Towards domain-invariant Self-Supervised Learning with Batch Styles Standardization},
  author={Scalbert, Marin and Vakalopoulou, Maria and Couzinie-Devy, Florent},
  booktitle={The Twelfth International Conference on Learning Representations},
  year={2023}
}
```

## :envelope: 5. Contact us

If you have any technical issues with the code or questions regarding the paper, you can contact us via this [mail adress](mailto:marin.scalbert@centralesupelec.fr).